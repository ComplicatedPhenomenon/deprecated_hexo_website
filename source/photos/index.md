---
title: Photos
date:
type: "photo"
comments: false
---
#### Watercolors
Contrary to photos, paintings are a bit far from what something really looks like. Watercolors just took my breathe away, It is so vivid that makes me value life and recognize the world again. Everything becomes unique.

Abel Antonysamy

![](http://www.shuicaimi.com/wp-content/uploads/2016/09/shuicai20160926044142.jpg)

Myoe Win Aung

<img src="http://www.kulturologia.ru/files/u6205/62054689.jpg" alt="http://www.kulturologia.ru/files/u6205/62054689" style="width: 500px;"/>

[John Salminen](http://johnsalminen.com)

#### People
There is aways some people inspire you to be a better person.

[Aaron Swartz](http://www.aaronsw.com/)

<img src="http://www.slate.com/content/dam/slate/blogs/future_tense/2013/01/13/Aaron_Swartz.jpg.CROP.cq5dam_web_1280_1280_jpeg.jpg" alt="http://www.slate.com/content/dam/slate/blogs/future_tense/2013/01/13/Aaron_Swartz.jpg.CROP.cq5dam_web_1280_1280_jpeg" style="width: 500px;"/>

[Berners-Lee](https://www.w3.org/People/Berners-Lee/Kids.html)

<img src="http://www.redicecreations.com/ul_img/4812timbernerslee.jpg" alt="http://www.redicecreations.com/ul_img/4812timbernerslee" style="width: 500px;"/>
