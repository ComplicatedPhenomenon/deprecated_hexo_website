---
title: Transfer from Python2 to Python3
date: 2018-01-27 11:30:33
tags: Tools
categories:
- Technology
- Method
description: Python Version Transfer
---
![](https://media.giphy.com/media/eBQiVZ358Waas/giphy.gif)
### Purpose:
I want to move Python2 to Python3
### Reason:
No more new features will be added to Python2.
<!--more-->
>Python 3 is regarded as the future of Python and is the version of the language that is currently in development. A major overhaul, Python 3 was released in late 2008 to address and amend intrinsic design flaws of previous versions of the language. The focus of Python 3 development was to clean up the codebase and remove redundancy, making it clear that there was only one way to perform a given task.

[Reference](https://www.quora.com/Should-I-move-from-Python-2-7-to-3)

Two Python, not the newest version, are on my computer

### Problems encountered
```
$ sudo apt-get upgrade pip3

E: Failed to fetch https://deb.opera.com/opera-stable/pool/non-free/o/opera-stable/opera-stable_47.0.2631.80_amd64.deb  Operation too slow. Less than 10 bytes/sec transferred the last 120 seconds

E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
```

```
$ sudo apt-get  update
Err:18 http://ppa.launchpad.net/fcitx-team/nightly/ubuntu xenial/main amd64 Packages
  404  Not Found
  Ign:19 http://ppa.launchpad.net/fcitx-team/nightly/ubuntu xenial/main i386 Packages
  Ign:20 http://ppa.launchpad.net/fcitx-team/nightly/ubuntu xenial/main Translation-en
  Fetched 102 kB in 23s (4,366 B/s)
  Reading package lists... Done
  W: The repository 'http://ppa.launchpad.net/fcitx-team/nightly/ubuntu xenial Release' does not have a Release file.
  N: Data from such a repository can't be authenticated and is therefore potentially dangerous to use.
  N: See apt-secure(8) manpage for repository creation and user configuration details.
  E: Failed to fetch http://ppa.launchpad.net/fcitx-team/nightly/ubuntu/dists/xenial/main/binary-amd64/Packages  404  Not Found
  E: Some index files failed to download. They have been ignored, or old ones used instead.
 ```
 I can't solve this problem by myself, after *google* the relevant problem, no same or very close problem found. Then I about to do something else, but I think it's better to figure out my problem. Every time when you ask a question on *SE*, you are supposed to know what you are doing or you should be ashamed of yourself. On the site of *Ask Ubuntu*, I find a problem which is highly relevant with mine. However, my mind was not as sharp as I expected. So I guess it's still difficult for me to handle my problem,  then I try a little bit, At least correctly understand [the reasonable man's words](https://askubuntu.com/questions/858374/apt-get-update-missing-release-file).three times tried the steps below:

 ```
 sudo add-apt-repository --remove ppa:ubuntu-audio-dev/ppa
 sudo apt-get update
 ```
 Nothing changed, I still got the same error at the very first. I eagerly to cite this problem and feedback my confusion, then somehow, I understand I need to change a little bit.
 ```
 sudo add-apt-repository --remove ppa:fcitx-team/nightly
 sudo apt-get update
 ```
 It worked as expected. Now I am updating `pip3` but I found the speed at some certain point become extremely slow, it seems I will never finish my task. When I am about to ask this question, I am surprised to find out that that slow point is about
 ```
 Get:1 https://deb.opera.com/opera-stable stable/non-free amd64 opera-stable amd64 47.0.2631.80 [49.9 MB]
 ```
 As it's blocked by the Great fire wall, I suddenly understand that
 *opera-stable* shouldn't be upgraded now. So my next question is: [How to prevent updating of a specific package?](https://askubuntu.com/questions/18654/how-to-prevent-updating-of-a-specific-package)

 ```
 echo "opera-stable hold" | dpkg --set-selections
 ```
Did it work?

It did work, I finish  without problem any more.
```
sudo apt-get upgrade python3
```
But I still fail to achieve my goal.

If I'm awake, then it won't took so much time for me to figure out what was going on.

It's hard to choose whether I should spend more time on using *numpy* or learning how to write code which can implement that function in *numpy*. Both are important, but I only got limited time.

I want to figure out the reason why I keep doing something which makes me feel regret again and again? Take a lesson from the past so that you won't make the same mistakes again.

Why I keep doing it? I'm looking for trouble very often.

[Warrior](http://www.explainedlyrics.com/imagine-dragons-warriors-lyrics/)
