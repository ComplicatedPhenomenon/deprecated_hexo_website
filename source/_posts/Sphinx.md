---
title: Sphinx
date: 2018-03-31 10:27:21
tags: Sphinx
categories:
- Technology
- Tools
description: Blog is not a good place to write professional notes, the structure there is less clear. You can share a new idea in a blog, but to present a detail implementation, readthedocs is a good choice.
mathjax: false
---
## Sphinx
![](https://tse2-mm.cn.bing.net/th?id=OIP.s0rdf2m0Ycae5PG1hrZPngHaB4&p=0&o=5&pid=1.1)

<!--more-->

Sphinx was originally created for [the Python documentation](https://docs.python.org/), and it has excellent facilities for the documentation of software projects in a range of languages. Of course, this site ([Sphinx](http://www.sphinx-doc.org/en/master/)) is also created from *reStructuredText* sources using Sphinx!

Sphinx uses *reStructuredText* as its markup language, and many of its strengths come from the power and straightforwardness of *reStructuredText* and its parsing and translating suite, the [Docutils](http://docutils.sourceforge.net/).

[Sphinx FAQ](http://www.sphinx-doc.org/en/master/faq.html)

## quick start
```
/usr/bin
sphinx-apidoc      sphinx-autogen     sphinx-build       sphinx-quickstart
```
Sphinx comes with a script called sphinx-quickstart that sets up a source directory and creates a default conf.py with the most useful configuration values from a few questions it asks you. To use this, run:
```sh
$ sphinx-quickstart
Welcome to the Sphinx 1.7.2 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]:

Inside the root directory, two more directories will be created; "_templates"
for custom HTML templates and "_static" for custom stylesheets and other static
files. You can enter another prefix (such as ".") to replace the underscore.
> Name prefix for templates and static dir [_]:

The project name will occur in several places in the built documentation.
> Project name: woody
> Author name(s): Pixar


If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
http://sphinx-doc.org/config.html#confval-language.
> Project language [en]: 2018-05-13

The file name suffix for source files. Commonly, this is either ".txt"
or ".rst".  Only files with this suffix are considered documents.
> Source file suffix [.rst]:

One document is special in that it is considered the top node of the
"contents tree", that is, it is the root of the hierarchical structure
of the documents. Normally, this is "index", but if your "index"
document is a custom template, you can also set this to another filename.
> Name of your master document (without suffix) [index]:

Sphinx can also add configuration for epub output:
> Do you want to use the epub builder (y/n) [n]:
Indicate which of the following Sphinx extensions should be enabled:
> autodoc: automatically insert docstrings from modules (y/n) [n]:
> doctest: automatically test code snippets in doctest blocks (y/n) [n]:
> intersphinx: link between Sphinx documentation of different projects (y/n) [n]:
> todo: write "todo" entries that can be shown or hidden on build (y/n) [n]:
> coverage: checks for documentation coverage (y/n) [n]:
> imgmath: include math, rendered as PNG or SVG images (y/n) [n]:
> mathjax: include math, rendered in the browser by MathJax (y/n) [n]:
> ifconfig: conditional inclusion of content based on config values (y/n) [n]:
> viewcode: include links to the source code of documented Python objects (y/n) [n]:
> githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]:

A Makefile and a Windows command file can be generated for you so that you
only have to run e.g. make html instead of invoking sphinx-build
directly.
> Create Makefile? (y/n) [y]:
> Create Windows command file? (y/n) [y]:

Creating file ./conf.py.
Creating file ./index.rst.
Creating file ./Makefile.
Creating file ./make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file ./index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where builder is one of the supported builders, e.g. html, latex or linkcheck.

$ ls
_build  conf.py  index.rst  make.bat  Makefile  _static  _templates
```
运行 `sphinx-quickstart` 命令后，在工作目录中会出现如下类似的文件。工作目录的列表

```
.
├── Makefile
├── _build　
├── _static
├── conf.py
└── index.rst```

让我们详细研究一下每个文件。
Makefile：编译过代码的开发人员应该非常熟悉这个文件，如果不熟悉，那么可以将它看作是一个包含指令的文件，在使用 make 命令时，可以使用这些指令来构建文档输出。
* \_build：这是触发特定输出后用来存放所生成的文件的目录。
* \_static：所有不属于源代码（如图像）一部分的文件均存放于此处，稍后会在构建目录中将它们链接在一起。
* conf.py：这是一个 Python 文件，用于存放 Sphinx 的配置值，包括在终端执行 sphinx-quickstart 时选中的那些值。
* index.rst：文档项目的 root 目录。如果将文档划分为其他文件，该目录会连接这些文件。
入门指南

此时，我们已经正确安装了 Sphinx，查看了默认结构，并了解了一些基本语法。不要直接开始编写文档。缺乏布局和输出方面的知识会让您产生混淆，可能耽误您的整个进程。
现在来深入了解一下 index.rst 文件。它包含大量的信息和其他一些复杂的语法。为了更顺利地完成任务并避免干扰，我们将合并一个新文件，将它列在主要章节中。
在 index.rst 文件中的主标题之后，有一个内容清单，其中包括 toctree 声明。toctree 是将所有文档汇集到文档中的中心元素。如果有其他文件存在，但没有将它们列在此指令下，那么在构建的时候，这些文件不会随文档一起生成。

我们想将一个新文件添加到文档中，并打算将其命名为 example.rst。还需要将它列在 toctree 中，但要谨慎操作。文件名后面需要有一个间隔，这样文件名清单才会有效，该文件不需要文件扩展名（在本例中为 .rst）。显示该列表的外观。在文件名距离左边距有三个空格的距离，maxdepth 选项后面有一个空白行。

Then I want to have a look how sphinx took effect.
```
path/to/project$ make html
```
* [使文档变得更有效并且可编写](https://www.ibm.com/developerworks/cn/opensource/os-sphinx-documentation/index.html)

I need a `template.rst` file. However, I have no idea about this language, luckily, I found the site built by sphinx has source code on its every page.

### Theme for Sphnix [...](http://www.sphinx-doc.org/en/master/theming.html)

* built-in theme

  To customize each theme, where the file is to be modified?

  ```
  $ ls /usr/share/sphinx/
  ext  locale  pycode  scripts  search  templates  texinputs  themes
  $ ls /usr/share/sphinx/themes/
  agogo  bizstyle  default  haiku   nonav    scrolls    traditional
  basic  classic   epub     nature  pyramid  sphinxdoc
  ```
  Configure `conf.py`

  ```
  ...
  html_theme = 'classic'
  ...
  html_theme_options = {
  "rightsidebar" : True,
  "stickysidebar" : True,
  "collapsiblesidebar" : True,
  "bodyfont" : 'cursive'
  }
  ...
  ```

  [CSS font family](https://www.w3.org/Style/Examples/007/fonts.en.html)
* non-built-in theme

## Advanced knowledge after familiar with the basic
### How Sphinx is designed for python documentation?
> Originally, Sphinx was conceived for a single project, the documentation of the Python language. Shortly afterwards, it was made available for everyone as a documentation tool, but the documentation of Python modules remained deeply built in – the most fundamental directives, like function, were designed for Python objects.


## Using Sphinx with *Read the Docs*
[https://readthedocs.org](https://readthedocs.org) is a documentation hosting service based around Sphinx. They will host sphinx documentation, along with supporting a number of other features including version support, PDF generation, and more. The Getting Started guide is a good place to start.

![](https://media.giphy.com/media/NMjRRZOo9XtZe/giphy.gif)
