---
title: Markdown and It's Extensions
date: 2018-03-26 11:17:25
tags: Markdown
categories:
- Technology
- Tools
description: Tools for Writing scientific papers with less tedious work
mathjax: false
---
### Principle behind displaying complex content on the web <!--more-->
* *Mathjax* | [A JavaScript display engine for mathematics that works in all browsers. No more setup for readers. It just works.](https://www.mathjax.org/#gettingstarted)
  * MathML(MAthmaticas MArkup Language) and Latex

  Rendering *MathML* and *Latex* Using *MathJax*

  A more authoritative explanation.
  [Putting mathematics on the Web with MathJax](https://www.w3.org/Math/MJ/Overview.html)

   [click](https://stackoverflow.com/a/49452142)

   Here is a sample document that does both:

   ```html
   <!DOCTYPE html>
   <html>
   <head>
   <title>TeX and MathML in one file</title>
   <script   src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML"></script>
   </head>
   <body>

   Both \(1+x\) and <math><mi>x</mi><mo>+</mo><mn>1</mn></math> are the same.

   </body>
   </html>
   ```

   Your problem is that you are not using the correct configuration file. The default configuration is for when you run *MathJax* from your own server and can eat the default configuration. You can't do the from the CDN, so load one of the pre-defined configurations that processes both *TeX* and *MathML*.

There is a multitude of options to display mathematics on the web including *MathJax*, *Katex*, *MathML* and *Latex2Image*. Which one is right for you? [click](https://www.bersling.com/2016/05/10/displaying-math-on-the-web/)

You can't really compare *MathJax* and *MathML* as they are different things. [click](https://tex.stackexchange.com/a/2722/127717)

* *jQuery*  is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use *API* that works across a multitude of browsers. With a combination of versatility and extensibility, *jQuery* has changed the way that millions of people write JavaScript.

### Our Aim

*Markdown* and *Pandoc* and *Jupyterbook* *Read the Docs* can be used to display your notes in a readable way.
* [Academic Markdown and Citations](http://www.chriskrycho.com/2015/academic-markdown-and-citations.html)
* Presentation slides with Jupyter Notebook
* [Read the Docs](https://readthedocs.org)

Building an academic CV.

* [Markdown+CSS](https://blm.io/blog/markdown-academic-cv/)

* [Editing a CV in markdown with pandoc](https://blog.chmd.fr/editing-a-cv-in-markdown-with-pandoc.html)
