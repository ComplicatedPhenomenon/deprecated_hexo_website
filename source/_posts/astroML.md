---
title: astroML
date: 2018-03-31 09:59:48
tags: astroML
categories:
- Technology
- Library
description: The progress I made in learning from the book Statistics, Data Mining, and Machine Learning in Astronomy
mathjax: True
---
Machine learning has a wide spread application in Astronomy, I want to know the detail how it actually work. then I found a book *Statistics, Data Mining, and Machine Learning in Astronomy* by Zeljko Ivezic, Andrew Connolly, Jacob VanderPlas, and Alex Gray.

Accompanying with this book, there is also other relevant resources.
* A python module.
  * astroML is designed as a repository for fast and well-tested code for the analysis of astronomical data. envisioned to serve as a community resource.
  * Instead of redeveloping fast algorithms, astroML make full use of the well test library.
  * astroML strives to bring the astronomical community closer to the idea of reproducible research.
* A website introducing the module. visit my another post [ Sphinx](https://complicatedphenomenon.github.io/2018/03/31/Sphinx/#more).

<!--more-->
## Install astroML and make use of structured documentation
After visiting the repository for module astroML. I know
* How to structure a package. A module and its user guide. Then make the generated user guide a website.
* How to provide the necessary detail. Like the versions for every package.
  Sphnix==1.1.3

```
(astroMLSphnix1.1.3) wm@vampire:~/Projects/astroML/doc$ make html
sphinx-build -b html -d _build/doctrees   . _build/html
Running Sphinx v1.1.3

Extension error:
Could not import extension gen_rst (exception: No module named matplotlib)
Makefile:45: recipe for target 'html' failed
make: *** [html] Error 1
```

If I install astroML via `python setup.py install`, why *matplotlib* is still lost? how I am supposed to install the proper version *matplotlib* ?
```
(astroMLSphnix1.1.3) wm@vampire:~/Projects/astroML$ ls
astroML       CHANGES.rst        doc          Makefile     setup.py
book_figures  CITATION           examples     MANIFEST.in  TODO.txt
build         compare_images.py  LICENSE.rst  README.rst
(astroMLSphnix1.1.3) wm@vampire:~/Projects/astroML$ conda list
# packages in environment at /home/wm/anaconda3/envs/astroMLSphnix1.1.3:
#
# Name                    Version                   Build  Channel
astroML-0.4               git                       <pip>
...
pip                       10.0.1                   py27_0  
python                    2.7.15               h1571d57_0  
...
zlib                      1.2.11               ha838bed_2  
```
```
$ conda  list -n manim
# packages in environment at /home/wm/anaconda3/envs/manim:
#
# Name                    Version                   Build  Channel
...
numpy                     1.11.1                    <pip>
pip                       10.0.1                   py27_0  
python                    2.7.15               h1571d57_0  
...
zlib                      1.2.11               ha838bed_2  
```
I have created several environments which is a waste since some of them separately used same packages.

![](https://media1.giphy.com/media/zuHo4NrwPRPLa/200w.gif)
