---
title: Django v.s web.py
date: 2018-06-15 06:25:41
tags: web development
categories:
  - Technology
  - Tools
description: You'd better to be good at at least one development framework. It's helpful.
mathjax: false
---
The way of building feature-rich applications gets considerably more complicated. For any web developers or designers, a web framework is a blessing in disguise as it offers them flexibility and multiple options to explore and save their invaluable time.  If these frameworks were not there, web development could have, literally, become difficult as you need to take care of so many things.

Here we introduce 2 backend web development frameworks.
<!--more-->
## Webpy
![](http://webpy.org/static/webpy.gif)
Set up an environment for running webpy.

Nevertheless, I can't fully experience the usage of webpy since it's depends on PostgreSQL which supports part of the LTS Ubuntu version.


## Installing from a repository
```
$ git clone https://github.com/webpy/webpy.git
```
There is a document named docs written in rST language. I need to take a look at it, The appropriate way is to decode it with *sphnix* which I have installed already, nevertheless, that is a different version and has contradictions with the one Aaron used.
```
(webpy) wm@vampire:...$ pip install sphnix

```

## web.py and Django
web.py criticize  *Django*.

* Django lets you write web apps in Django. TurboGears lets you write web apps in TurboGears. Web.py lets you write web apps in Python.

The advantage of Django.

There is a fact that if you are immature to using the tools, you see little about its limitation. 

Contents to be continued...

This post is even helpless for myself.
![](https://media.giphy.com/media/hJ6Ff8TrrtG92/giphy.gif)
