---
title: Vegas
date: 2018-06-15 06:26:14
tags: algorithm
categories:
- Technology
- Library
description: An algorith for multi-dimensional integration
mathjax: false
---
Vegas have been implemented in several ways. including
* Fortran language
* C language
* Cython

Here we put emphasis on Cython.
<!--more-->
## Cython
Cython is a language or what
* A python module?
* A mixture language?
* An optimising static compiler?

The Cython language is a superset of the Python language that additionally supports calling C functions and declaring C types on variables and class attributes. This allows the compiler to generate very efficient C code from Cython code. The C code is generated once and then compiles with all major C/C++ compilers in CPython 2.6, 2.7 (2.4+ with Cython 0.20.x) as well as 3.3 and all later versions. We regularly run integration tests against all supported CPython versions and their latest in-development branches to make sure that the generated code stays widely compatible and well adapted to each version. PyPy support is work in progress (on both sides) and is considered mostly usable since Cython 0.17. The latest PyPy version is always recommended here.

Cython language is compiled by Cython compiler?

**Cython and CPython**

> Cython is often confused with CPython (mind the P), but the two are very different. CPython is the name of the standard and most widely used Python implementation. CPython’s core is written in the C language, and the C in CPython is meant to distinguish it from Python the language specification and Python implementations in other languages, such as Jython (Java), IronPython (.NET), and PyPy (Python implemented in Python!). CPython provides a C-level interface into the Python language; the interface is known as the Python/C API. Cython uses this C interface extensively, and therefore Cython depends on CPython. Cython is not another implementation of Python—it needs the CPython runtime to run the extension modules it generates.

```
$ Cython
Cython (http://Cython.org) is a compiler for code written in the
Cython language.  Cython is based on Pyrex by Greg Ewing.

Usage: Cython [options] sourcefile.{pyx,py} ...
```

## Vegas repository
```
$ tree vegas/src
vegas/src/
├── vegas
│   ├── __init__.py
│   ├── _vegas.c
│   ├── _vegas.pxd
│   ├── _vegas.pyx
│   └── _version.py
└── vegas.pxd
```
I guess `_vegas.c` is
Cython compile Cython language to C source code.


Python scripts may have one of several file extensions. Each file extension has a special meaning and purpose.

* .pyc - compiled script (Bytecode)
* .pyo - optimized pyc file (As of Python3.5, Python will only use pyc rather than pyo and pyc)
* .pyw - Python script to run in Windowed mode, without a console; executed with pythonw.exe
* .pyx - Cython src to be converted to C/C++
* .pxd - Cython script which is equivalent to a C/C++ header
* .pyd - Python script made as a Windows DLL
...

A larger list of additional Python file-extensions (mostly rare and unofficial) can be found at http://dcjtech.info/topic/python-file-extensions/


> The pipeline comprises two stages. The first stage is handled by the Cython compiler, which transforms Cython source into optimized and platform-independent C or C++. The second stage compiles the generated C or C++ source into a shared library with a standard C or C++ compiler. The resulting shared library is platform dependent. It is a shared-object file with a .so extension on Linux or Mac OS X, and is a dynamic library with a .pyd extension on Windows. The flags passed to the C or C++ compiler ensure this shared library is a full-fledged Python module. We call this compiled module an extension module, and it can be imported and used as if it were written in pure Python.

![](https://media.giphy.com/media/o7cWsa4hC2rPa/giphy.gif)
