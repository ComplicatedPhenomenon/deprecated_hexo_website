---
title: MadGraph5_aMC@NLO
date: 2018-05-18 22:16:22
tags: MadGraph5_aMC@NLO
categories:
- Technology
- Method
description: From almost knowing nothing to become a qualified new user
mathjax: false
---
![](https://media.giphy.com/media/l0Ex5yl7V4UEBmhcQ/giphy.gif)
## Resource
* [MadGraph School Shanghai 2015](http://www.physics.sjtu.edu.cn/madgraphschool/node/29)
* [Lectures on the MadGraph framework](https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MGTutorial)


<!--more-->

## MG5_aMC_v2_5_5 skeleton
* aloha
* bin
* Template
* PLUGIN		 
* tests
* Delphes      
* HEPTools  
* MadSpin	 
* input    
* INSTALL   
* models
 * RS etc.
* madgraph
  * core
* HELAS  (HELicity Amplitude Subroutine library)

Structure Flowcharts
![](https://cp3.irmp.ucl.ac.be/projects/madgraph/raw-attachment/wiki/MGChart/MG_chart.jpg)


Most simple MadGraph refers to parton level, If you want to simulate more realistic situation, which parton shower,hardronization etc. are included. Then more software packages are needed.

The need for better simulation tools has spurred a very intense activity
* Automated matrix element generation (MADGRAPH5, etc.)
* Higher-order computations (aMC@NLO , etc.)
* Parton showering and hadronization (PYTHIA, HERWIG , etc.)
* Matrix element - parton showering matching
* Merging techniques (MLM, CKKW, FxFx, UNLOPS, etc.)

### Install additional package
Install additional via package manager script(`mg5_aMC install XX`) but also possible by manual download.


```
MG5_aMC>help install
syntax: install Delphes|MadAnalysis4|ExRootAnalysis|update|SysCalc|Golem95|PJFry|QCDLoop|Maddm|pythia8|zlib|boost|lhapdf6|lhapdf5|collier|hepmc|mg5amc_py8_interface|ninja|oneloop|MadAnalysis5|MadAnalysis

MG5_aMC>install pythia8
   You are installing 'pythia8', please cite ref(s): arXiv:1410.3012.
Downloading the HEPToolInstaller at:
   http://madgraph.physics.illinois.edu/Downloads/HEPToolsInstaller/HEPToolsInstaller_V106.tar.gz
LHAPDF was not found. Do you want to install LHPADF6? (recommended)  y/n >[y] [60s to answer]
>y
...
failed
```
`Delphes` depends on `root`  
#### LHAPDF6
#### PYTHIA8

### Hadronization

* Herwig is a multi-purpose particle physics event generator.
* PYTHIA is a program for the generation of high-energy physics events.
 * Basic Installation procedure. [...](http://home.thep.lu.se/~torbjorn/Pythia.html)
 * A Doxygen representation of the code is available. It offers a quick way to look up classes and methods, but is not a replacement for the manual.
* Sherpa is a Monte Carlo event generator that provides complete hadronic final states in simulations of high-energy particle collisions.


如果你想计算新物理模型，你得写新的软件包，FeyRules可以自动生成这种包。MadGraph自带一些常用的新物理模型包。

There are many great programs for calculating Feynman diagrams

* CalcHEP/CompHEP
* FeynArts/FormCalc
* MadGraph
* Sherpa
* ...

* Each has its own strengths and weaknesses
* Each has its own model-file format
There is one tool that writes model-files for each of these.

MAdGraph是用Python写的框架，但在计算上很多是用C,Fortran实现的。

Details of the computation
* Evaluation of matrix-element
* Phase-Space integration

### Citation
In addition to the main paper above, some papers should or may be cited as well, depending on which aspect(s) of a given simulation are particularly relevant and worth stressing. [Citation paper](http://amcatnlo.web.cern.ch/amcatnlo/list_refs.htm#Native)


### It only support Python2
[Python refer](https://stackoverflow.com/q/7237415/7583919)

```
alias python="/usr/bin/python2.7
```
[Failed to access python version of LHAPDF](https://answers.launchpad.net/mg5amcnlo/+question/407669)

Do you have the following PATH
```
~/MG5_aMC_v2_5_2/HEPTools/lhapdf6/lib/python2.6/site-packages
```
Mine
```
~/MG5_aMC_v2_6_2/HEPTools/lhapdf6/lib$ ls
libLHAPDF.a  libLHAPDF.la  libLHAPDF.so  pkgconfig  python3.6
```


## MadGraph Q&A [...](https://answers.launchpad.net/mg5amcnlo/+question/268076)
> Dear Sergei,

> In the ADD model the Graviton is not a single state, but rather a superposition of many nearly degenerate particles. Therefore, one cannot generate such a process directly with madgraph5_aMC@NLO. What is possible, is to have the (sum of) Graviton states as an intermediate resonance, if you create/find a model for this. It might be available from the FeynRules website --I haven't checked.

> Best regard,
> Rikkert

## Invent your own Feynman Rule through FeynRules
To use FeynRules, the user must start by entering the details of the new model in a form that can be parsed by FeynRules. First of all, this means that, since FeynRules is a Mathematica package, the model must be written in a valid Mathematica syntax. Secondly, FeynRules specifies a set of special variables and macros for the user to enter each aspect of a new model.

After the model description is created and the Lagrangian constructed, it can be loaded into FeynRules and the Feynman rules obtained.

After the Feynman rules have been obtained, the user is typically interested in the phenomenology of the model, which requires the evaluation of Feynman diagrams. There are many tools that allow to evaluate Feynman diagrams automatically.
```
?WriteUFO
Writes the Python output files for the model.
```

```
ls .Mathematica/Applications/FeynCalc/
AddOns        Dirac          FCConfig.m  Feynman        NonCommAlgebra  QCD
Changelog.md  Documentation  fc.m        Kernel         PacletInfo.m    Shared
COPYING       Examples       FCMain.m    LoopIntegrals  Pauli           SUN
Database      ExportImport   FeynCalc.m  Lorentz        Phi             Tables
```

[An example might help vh@nnlo](http://particle.uni-wuppertal.de/harlander/software/vh@nnlo/)

If you want to calculate cross section at the parton level, you only need MAdGraph/MadEvent

If you are interested in something more(like hadronization, detector simulation, also so on) you need all  

[Parton shower Monte Carlo event generators](http://scholarpedia.org/article/Parton_shower_Monte_Carlo_event_generators)

MadGraph
```
mg5_aMC> output ~/pptottbar
```
```
pptottbar/$ tree -L 1
.
├── bin
├── Cards
├── crossx.html
...
```

```
$ tree Cards/
Cards/
├── delphes_card_ATLAS.dat
...
├── me5_configuration.txt
├── param_card.dat                      ! modify top mass ...
├── param_card_default.dat
...
├── run_card.dat                        ! modify beam energy
└── run_card_default.dat

0 directories, 30 files
```

![](https://media0.giphy.com/media/l41m3Yq3dqnXeGfQY/200.gif)
