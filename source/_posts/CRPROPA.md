---
title: CRPROPA
date: 2018-09-07 19:57:06
tags: Software
categories:
- Technology
- Tools
description: an astrophysical simulation framework for propagating extraterrestrial ultra-high energy particles
mathjax: false
---
## System-applied python and Anaconda-supplied python
+ [x] Call a specific python interpreter
+ [x] List the packages under a certain environment

<!--more-->
```
$ /usr/bin/python3
Python 3.6.5 (default, Apr  1 2018, 05:46:30)
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()
$ python3
Python 3.6.5 |Anaconda custom (64-bit)| (default, Apr 29 2018, 16:14:56)
[GCC 7.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>exit()
$ /usr/bin/pip2 list --format=legacy
asn1crypto (0.24.0)
...
mpmath (1.0.0)
numpy (1.13.3)
...
```

Through the way installing CRPropa. During practise, I learn universal basic skill of install a package from source code (compared to pre-built tarball).

* `./configure ` is usually equal to `./configure --prefix=/user/local`, we don't use default setup this time.
* ./configure --prefix=$CRPROPA_DIR


## Install dependency once for all

**When your time is limited and you are uncertain you shall handle all the tricky problems might encountered, I suggest to obey the guide provided by experienced pioneer.**

Even though you I like to replace `virtualenv` with `conda`, I still use `virtualenv` for security.

So the demonstration is depend on linux, so I tested on Ubuntu18.04.

```
$ sudo apt install python-virtualenv build-essential git cmake swig  gfortran python-dev fftw3-dev zlib1g-dev libmuparser-dev libhdf5-dev pkg-config
[sudo] password for wm:
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Note, selecting 'libfftw3-dev' instead of 'fftw3-dev'
build-essential is already the newest version (12.4ubuntu1).
cmake is already the newest version (3.10.2-1ubuntu2).
gfortran is already the newest version (4:7.3.0-3ubuntu2).
pkg-config is already the newest version (0.29.1-0ubuntu2).
pkg-config set to manually installed.
python-dev is already the newest version (2.7.15~rc1-1).
python-dev set to manually installed.
zlib1g-dev is already the newest version (1:1.2.11.dfsg-0ubuntu2).
zlib1g-dev set to manually installed.
git is already the newest version (1:2.17.1-1ubuntu0.1).
The following packages were automatically installed and are no longer required:
  python-paramiko python-pyasn1
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  hdf5-helpers libaec-dev libaec0 libfftw3-bin libfftw3-long3 libfftw3-quad3
  libhdf5-100 libhdf5-cpp-100 libjpeg-dev libjpeg-turbo8-dev libjpeg8-dev
  libmuparser2v5 libsz2 swig3.0
Suggested packages:
  libfftw3-doc libhdf5-doc swig-doc swig-examples swig3.0-examples swig3.0-doc
The following NEW packages will be installed
  hdf5-helpers libaec-dev libaec0 libfftw3-bin libfftw3-dev libfftw3-long3
  libfftw3-quad3 libhdf5-100 libhdf5-cpp-100 libhdf5-dev libjpeg-dev
  libjpeg-turbo8-dev libjpeg8-dev libmuparser-dev libmuparser2v5 libsz2
  python-virtualenv swig swig3.0
0 to upgrade, 19 to newly install, 0 to remove and 44 not to upgrade.
Need to get 8,372 kB of archives.
After this operation, 42.9 MB of additional disk space will be used.
Do you want to continue? [Y/n] Y

$ virtualenv $CRPROPA_DIR
Running virtualenv with interpreter /usr/bin/python2
New python executable in /home/wm/.virtualenvs/crpropa/bin/python2
Also creating executable in /home/wm/.virtualenvs/crpropa/bin/python
Installing setuptools, pkg_resources, pip, wheel...done.
```
Or you can install dependency one by one manually. However, you might encounter problem which has more profound reason.
 [`Eg. Swig not found during compilation`](https://github.com/CRPropa/CRPropa3/issues/171).



### Problem worth mentioning
```
runtime library [LIBRARY]  in /usr/lib/[PATH] may be hidden by files in :/home/[USER]/anaconda3/lib
```
* [Prompt 1](https://github.com/pism/pism/issue/356)
* [Prompt 2](https://github.com/mopheus/issue/3)

  > Anaconda install itself as the first entry of the `PATH`. Therefore, cmake find the Anaconda library first. To avoid this. one should remove anaconda from the `PATH` before configuring CRPropa.

![](https://media.giphy.com/media/Lr132Tx2IwD0A/giphy.gif)
