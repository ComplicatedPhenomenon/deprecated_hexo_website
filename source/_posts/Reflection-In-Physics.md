---
title: Reflection In Physics
date: 2018-01-27 11:30:33
tags: Thoughts
categories:
  - Notes
  - Physics
description: It's unwise to begin to do anything without preparation, not perception of course, especially for your career, I am still filled with confusion and anxiety.
---
![](http://www.bibhasde.com/wittenlorentz.jpg)

<!--more-->

I decided to endeavor pursuing after theoretical physics long ago depending on very limited knowledge on physics and things about it, then as I am receiving large amount of professional trainings, I become a little bit numb, not passionate anymore. I have been beaten down by difficulties. It's extremely hard to fully understand concepts in physics, as a master candidate, I fail to live up to my expectation. Then I found something inspiring and essential from what Edward Witten had been through. [pdf](https://www.sns.ias.edu/ckfinder/userfiles/files/ComemorativeLecturePopular(1).

There is very few respectful experts who have their own academia style by my side, with whom I seldom know their thoughts directly from themselves. So I search a lot to see what a real physicist say about.

> Anyway, by November 1974, I had just about learned enough about elementary particles that I could understand what the excitement was about and what people were saying, but not quiet enough to participate prominently...

>    I gained a couple of things from this experience, even though I was not able to solve the problem I wanted. One was negative. I learned the hard way what I regard as one of the most important things about doing research.One needs to be pragmatic. One cannot have too much of a preconception of what problem one aims to solve. One has to be ready to take advantage of opportunities as they arise.

>    Reluctantly, I had to accept that the problem of quark confinement that I wanted to solve was too difficult.To make any progress at all, I had to lower my sights considerably and consider much more limited problems.

>    At Harvard, I learned a lot from many of the senior professors, originally the physicists and then some of the mathematicians as well.There were certain fundamental topics in physics that I had had trouble understanding as a graduate student. I think Steve thought that many of the physicists had some of the same confusions I did. Whenever one of these topics came up at a seminar, he would give a small speech explaining his understanding. After hearing these speeches a number of times, I myself gained a clearer picture.

>    When one is doing research, the trick is to find a problem simple enough that one can solve it but interesting enough that solving it is worthwhile. Seiberg and I managed to do this by finding quantum field theories that were simple enough that we could solve them and subtle enough so that we could learn useful lessons by solving them. Among other things, Seiberg-Witten theory enabled me to finally make a small contribution to the understanding of quark confinement, as I had dreamed to do as a student. It is instructive to look back and realize how far out of reach this contribution would have been in my student days, when I had first worked on this problem.


Not only can we see how he manage to figure out his own way in doing physics,but also why he can manage.

Even though we have extremely convenient way of acquiring  answers　from the Internet today, we might possibly turn into be a wise person, however, we might become lazy in thinking on our own mind or lose the ability to think even worse.

![](https://media.giphy.com/media/PQaRWf6SOtCAU/giphy.gif)
