---
title: Cadabra
date: 2018-06-15 08:07:34
tags: symbolic computation
categories:
  - Technology
  - Tools
description: Cadabra is a symbolic computer algebra system (CAS) designed specifically for the solution of problems encountered in field theory.
mathjax: false
---

Cadabra set an excellent example of building a software, which could work independently and also can be integrated into other tools.
<!--more-->

## Cadabra
>  The problem is that if you run an Anaconda-supplied python, you will not be able to import packages which are supplied in the form of .deb packages, or which were built by hand against the system-supplied python.

```
$ git clone https://github.com/kpeeters/cadabra2.git
Cloning into 'cadabra2'...
...
Receiving objects: 100% (272142/272142), 145.43 MiB | 1.48 MiB/s, done.
```
Astonished!

```
$ du -a -h --max-depth=1 cadabra2/| sort -hr
228M	cadabra2/
154M	cadabra2/.git
53M	    cadabra2/frontend
...
4.0K	cadabra2/CODE_OF_CONDUCT.md

$ du -a -h --max-depth=1 cadabra2/.git/ | sort -hr
154M	cadabra2/.git/
153M	cadabra2/.git/objects
...
4.0K	cadabra2/.git/branches


```

Is there a flag to pass to git when doing a clone, say don't clone the .git directory? If not, how about a flag to delete the .git directory after the clone?

* [Amature](https://stackoverflow.com/q/11497457)
* [Authority](https://www.atlassian.com/blog/git/handle-big-repositories-git)

  Two categories of big repositories

  If you think about it there are broadly two major reasons for repositories growing massive:

  * They accumulate a very very long history (the project grows over a very long period of time and the baggage accumulates)
  * They include huge binary assets that need to be tracked and paired  together with code.

Where would the module go when installing them via source code or dpkg?

Get a list of installed python modules. One direct way is

![](https://media3.giphy.com/media/zA4k9bPhzbyPC/200w.gif)
