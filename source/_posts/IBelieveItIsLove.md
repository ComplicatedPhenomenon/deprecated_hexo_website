---
title: I Believe It Is Love
date: 2018-01-27 11:30:33
tags: Love
categories:
- Life
- Diary
description: When you see my mind someday, I think you will walk toward me and hug me.
---
I see you

    Walking through a dream I see you
    My light in darkness breathing hope of new life
    Now I live through you and you through me, enchanting
    I pray in my heart that this dream never ends


    I see me through your eyes
    Living new life flying high
    Your love shines the way into paradise
    So I offer my life as a sacrifice
    I live through your love
    You teach me how to see all that's beautiful
    My senses touch your world I've never pictured
    Now I give my hope to you, I surrender
    I pray in my heart that this world never ends
    I see me through your eyes
    Living new life flying high
    Your love shines the way into paradise
    So I offer my life
    I offer my love for you
    When my heart was never open
    (And my spirit never free)
    To the world that you have shown me
    But my eyes could not envision
    All the colours of love and of life ever more, ever more


    I see me through your eyes
    Flying high
    Your love shines the way into paradise
    So I offer my life as a sacrifice
    And live through your love
    And live through your life

<!--more-->

#### 2017/03/06

Dear xx,

I want to tell you that I love you, Is this surprised you? I am not dare to admit this fact to you for two reasons.
* You don't have the similar feeling to me.
* I clearly know that I am not good enough to you.

So I won't interrupt your life, or when I tell you the feelings I have about you will become a burden on you. Nevertheless, I still could not help myself always thinking of you. When we begin to know each other gradually, okay, I confess that I get obsessed with you in next to no time after us having known each other. Too many nights you come in my dream, I feel so happy next morning, pretending everything is as usual when I stand next to you. Even I can feel that there is little hope between you and me, but you are so different for me that I  told my mom that maybe I am not a straight person, her attitude came as no surprise, her mind broke down, so now we just act that nothing had happened.  

> I'm going to fall in love with you, you don't have to love back with me. I'm going to give you my heart.

> If you accept me, I will forever love you, if you don't, I will miss you forever.


#### 2017/3/28
I've been waiting for you to call me to go back to dormitory, a wrong beginning make me upset since 4:00 PM.

My emotion become not so bad when you show up, but every time we say bye-bye I know the time we shall share together is becoming less. You  do not know there is a one care about you so much, she think you're the one to spend her rest life together. But what really happening is that she want you to live a happy life, keep her emotion aside, at least not interrupt you so you can make choice with entire freedom.

If I were something belong to you, I would feel happier than I am a girl. What nonsenses am I talking about?

You ask me whether I have a plan on Qingming, I can't tell you how excited I will be if you ask me to go somewhere with you. I am unwilling to confess the fact that you have no special feeling to me. But why do you ask me my time plan? You certainly ask me by the way. :(

As you are such a great person, people around you feel comfortable about the way you treat them, your trouble won't become a disaster unlike me. Therefore you will be happy all the way in your life.

When I was watching the film **WALL-E**, all of a sudden I feel like I was kind of like WALL-E the robot, may be when there are only two of us left on earth, you will know the affection that I feel on you is strong and real and will last until maybe the end of my life. I am too shy to show you my real emotion and also worried about losing you if I behave too much.

It's really beautiful when WALL-E and EVE dance in space.

![](https://tse2-mm.cn.bing.net/th?id=OIP.K_c4vfUen2FFyru0iz__ggHaEQ&w=300&h=300&p=0&o=5&pid=1.7)

#### 2017/3/29

If there is a possibility that you understand my intention one day, you definitely would wish me had lived a happy life before you know everything. So I determined to never ask too much from you even it's a little hard, I also live each day as usual, without disappointment, with a direction while living.  

Every time you touch my arm although it rarely happen, your gentle gesture freeze my entire body, undoubtedly , it were a song in my heart, I am wild with joy by your touch.

#### 2017/4/2

She is so sweet, her impressive unforgettable smile implanted in my mind. Forcefully I stop myself form asking her where she is now. What she is willing to do she will enjoy. I hope she enjoy her time.

I listen to the audio that I taped before this afternoon, from my weary voice I also hear my vivid life and good intention. People should reflect their selves regularly, They should get rid of the weary superficial difficulties and low-grade pleasure in order to save them from lost in life.

#### 2017/4/10

I want to stand in front of you and peacefully say that I really really love you, I would come out if you love me too.
#### 2017/4/17
Some time I miss you so much, I want to do anything else to distract my attention from intensively thinking of you, but it turns out to be useless. I try to make an excuse to come to see you, but these excuses seems so shallow. Even I had walked to your door preparing to say hello and just look at you for a while then I behaved myself. It's so hard, maybe my strong sense of emotion tonight is because in the night of yesterday JMM told me ZL has a girlfriend, they are brave, and they are younger, I ask myself why I can't tell the truth to people I am familiar with. Then the answer is that what if you refuse me then what can I do.

You have the privilege, which you deserve it, to choose whether promise me or say no to me. I totally trust you. (Your explicit refusal against the common sense, so we were used to be a little overwhelmed when you say no to our proposal, I think it's nothing about your personal attitude with other.) It's very likely to happen and I can feel it, you always directly refuse my invitation, however, you refuse everyone. Nevertheless, this just makes me love you more as you stay true to yourself. It  may seems that you are less considerate, but I hope you do everything with full heart instead of satisfying others including me.

#### 2017/5/3
If you ask me what am I doing these days, what I can say, actually, I am missing you. The eager of telling you that I love you is so strong, It's killing me, but with my last remaining reasonable mind, I can suffer from it but stop myself from telling you, God! I am really suffering, there is lots of pain in my heart. When I stand by your side, I know I will never have the courage to confess my real feeling. There is a voice in head warning me I will lose you no matter what I have done, because who I am but not anybody else.

One day, I may say this feeling which troubles me shouldn't have been a problem. I was so naive that couldn't look forward and go on. Let me be clear, I won't regret of loving anybody. I am struggle to be good enough to dare to tell the world I love that girl. I shouldn't admit this to her so easily.

#### 2017/5/15
Thank you for the considerable response when I tell you at last, You make the atmosphere easy for both of us. Thank you for several reasons.

* You make my monotonous world colorful. I know I am much more brave than the time I haven't met you, Then I make my mind to strive for being my self rather than any one else. :alien:
* From your inspiration, Now I am a practitioner instead of an idealist. I am more patient and smarter.

#### 2017/6/24
Today is your birthday, I thought it was yesterday, I will remember it. You will receive a gift

Anyhow, I let you go, and myself too. Laughters and enjoyable mood is so often appear when you are with your friends. I can bring you almost nothing. So I keep distance from you. I am afraid to ask you have a meal outside, will this make you embarrassed, I hope you always to be yourself.   

 Spock said to Kirk "I have been and always shall be your friend.", I couldn't say that to you. I am not your friend, I want to be your lover, however,this is never gonna to be happen,

I shouldn't get stuck in my life. So I look for help, it's not only happened to me...
people's true wish.


#### 2017/9/6

What am I gonna to say when we have a chance to freely talk. There is nothing I want to say at now, focus attention on your work, I will accompany with you to the airport, you'd better give me a hug, it's traditional people hug each other when see their friends off. If you don't notice I will ask for it.

We should deeply feel the love, love from family and friends. If we hardly feel it, then there is a mental problem. With love you can be a complete human.

![](https://media.giphy.com/media/iH9CCmVRN6J3i/giphy.gif)
