---
title: 'Hey, you, we speak the different language!'
categories:
  - Life
  - Random Thought
date: 2017-10-29 16:49:50
tags: Reflection
description: Wondering
---
<!--more-->

如果把一个人每天所说的话记录下来作分析，从它的用词，语言上面你就可以分析出不少东西，甚至比它自己更客观的了解它本身。

虽然google可以解决最硬的骨头，但是还不完美。比如人类追求情感的满足。我们就可以为史上有记录保存的资料建立专门的图书馆，用来让人走少一点的弯路，更好的认识感情。

小人得志最可怕。它看不见对一个有骨气的人，不吃嗟来之食，它看不见一个有品位，有潜力的人。很多小事早已经可以让我们以小见大，一些你熟知的人就是喜欢看热闹，对于即将到来的因为愚蠢导致的矛盾不去阻止，算不上真正的朋友。

平日里，不管发生什么稀罕的事，你的睡眠第一，控制意念。

把你躺下的奇妙想象转化成品味，坚定的选择。

![](https://media.giphy.com/media/Y234fAhLqgEIU/giphy.gif)
