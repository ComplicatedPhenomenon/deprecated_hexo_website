---
title: Hands-on practice with hexo
date: 2018-01-19 08:31:06
tags: Hexo
categories:
- Technology
- Method
description: Add some features I urgently demand to my website
mathjax: true
---
![](https://media.giphy.com/media/11zy4ZYNN7n9Ju/giphy.gif)
### Frameworks for building website
* *Django* makes it easier to build better Web apps more quickly and with less code.
* *Jekyll* transform your plain text into static websites and blogs.
* *Hexo* is a fast, simple & powerful blog framework
* *Hugo* is one of the most popular open-source static site generators. With its amazing speed and flexibility, Hugo makes building websites fun again.
<!--more-->

| Frameworks  |[Django](https://www.djangoproject.com)   |[Jekyll](https://jekyllrb.com)| [Hexo](https://hexo.io)   |[Hugo](http://gohugo.io)   |
|---          |---      |---   |---    |---|
| Written in  | Python  | Ruby| JavaScript |  [Go](https://golang.org)  |
|  Used for   | creating web applications, that is, software, normally backed by a database, that includes some kind of interactivity, that operates through a browser|Building static websites and blogs|   building Blog| Building  any kind of website including blogs, tumbles, and docs  |
|A brief introduction   | A Framework provides a structure and common methods for making this kind of software. | Jekyll is a simple, blog-aware, static site generator perfect for personal, project, or organization sites.|   Hexo is a fast, simple and powerful blog framework. |  Hugo is a static HTML and CSS website generator  |

~~The table is powered by [tablesgenerator](http://www.tablesgenerator.com/markdown_tables)~~

**Caution** We'd better visibly credit them on our site. Similar like  **Created using Sphinx 1.1.3. Design by Web y Limonada.**

### Problems I met during the first play with *hexo*.
Remember, Every problems you meet in the very beginning is very likely to be solved through *google*.

### Customize your theme
* You will screw up at first since you have poor knowledge, a heavy-weight, ugly configuration will be born.
* You will make it better since you are uncomfortable with it and you have a not bad taste.

I use choose theme *next* since it is fully documented, many people has devoted their time to make its tutorial more reliable and *next* itself is powerful. You will configure it with clear instruction, so that the website you built is satisfying. Moreover, you enjoy the whole process. [portal](http://volcfamily.cn/2016/10/03/Hexo%E4%B9%8BNexT%E4%B8%BB%E9%A2%98%E6%90%AD%E5%BB%BA%E5%8D%9A%E5%AE%A2%E8%AF%A6%E7%BB%86%E8%BF%87%E7%A8%8B/)

Basic effect example. [http://qixinbo.info/](http://qixinbo.info/)

Some needs:
* Add a background picture and some dynamic effect.
  [Doublemine](https://notes.wanghao.work/)
  * [Modify the CSS file](https://github.com/iissnan/hexo-theme-next/issues/556)
* Enable playing music.
* Launch the part for comments
* Count the number of visitors

Below the *next* directory, you can edit
* `config.yml`　
* `layout/_custom/sidebar.swig`
* `layout/_macro/sidebar.swig`
* `layout/_scripts/`
* `source/css/_custom/custom.styl`
* `source/css/_variables/custom.styl`
   You can change the the color of the background.
* ...

Useful reference
* [打造个性超赞博客Hexo+NexT+GithubPages的超深度优化](https://reuixiy.github.io/technology/computer/computer-aided-art/2017/06/09/hexo-next-optimization.html)
* [Hexo high level tutorial](http://cherryblog.site/Hexo-high-level-tutorialcloudmusic,bg-customthemes-statistical.html)
* [hexo的next主题个性化配置教程](http://shenzekun.cn/hexo的next主题个性化配置教程.html)

**Upgrade theme-next**  
Since I modify several files. When I want to upgrade next from *V5.1.4* to *V6.0*
It is a suffering. Installing a brand new one requires me to repeat what has been
done before. So I just install a parallel one and compare the difference between
these two repos. I didn't find a workable way.

Then I rename the previous *next*, and then install the new one, then I found
the difference easily.
[Instructions](https://github.com/theme-next/hexo-theme-next/blob/master/docs/zh-CN/UPDATE-FROM-5.1.X.md)

Besides *next* Wizardforceful created an theme  [cyanstyle](http://blog.csdn.net/wizardforcel/article/details/40684953)

### HTML CSS and JavaScript
The file extension *.swig*, *.styl*, *.js* is confusing to me at first. When I know the basic usage of the language used to build web page, things become much clear.

### Why RSS? Benefits and Reasons for using RSS

RSS solves a problem for people who regularly use the web. It allows you to
easily stay informed by retrieving the latest content from the sites you are
interested in. You save time by not needing to visit each site individually.
You ensure your privacy, by not needing to join each site's email newsletter.
The number of sites offering RSS feeds is growing rapidly and includes big
names like Yahoo News.


[RSS](https://answers.yahoo.com/question/index?qid=20071106013613AACVAGt)

[Enable RSS](https://segmentfault.com/a/1190000002632530)
### Enable [*MathJax*](https://en.wikipedia.org/wiki/MathJax)
Reliable instructions can be found in `/theme/next/docs/MATH.md`
```
npm un hexo-renderer-marked --save
npm i hexo-renderer-kramed --save # or npm i hexo-renderer-pandoc --save
```
Then you just need to turn on enable of math and choose a render engine for it (located in `next/_config.yml`):

```yml
math:
  enable: true

  # Default(true) will load mathjax/katex script on demand
  # That is it only render those page who has 'mathjax: true' in Front Matter.
  # If you set it to false, it will load mathjax/katex srcipt EVERY PAGE.
  engine: mathjax
```
Then it still doesn't work, It cost me hours to figure out the reason. The solution is already there actually. If I were smarter, then I would found the problem earlier. It teach me a lesson. If something were supposed to work easily but you get stuck. Then you need to be careful to exclude what might unlikely happen, such as the routine  introduced the error even you had repeated it again and again, moreover, you could find out the possible reason that cause the  bad situation in a short time.

**Displayed formula example**
$$
B_\nu(\lambda,T)=\frac{2hc^2}{\lambda^5}\frac{1}{e^{\frac{hc}{\lambda k_B T}}-1}
$$
![](https://tse2.mm.bing.net/th?id=OIP.txOKbD9FxksvyTxx-viDJgHaEz&pid=Api)

**Inline formula example**

Covariant form of Dirac Equation    $ i \hbar \gamma^\mu \partial_\mu \psi - m c \psi = 0$

### 在移动设备下启用NexT主题的目录页面和回到顶部按钮
[reuixiy's method](https://leaferx.online/2017/02/05/EnableTOConMobile/)

Bad experience! I give the option up.

### The series problem ever was solved in this way:
```sh
$ npm ls --depth 0
hexo-site@0.0.0 /home/wm/Playground/BuildABlogSite
├── hexo@3.5.0
├── hexo-deployer-git@0.3.1
├── hexo-generator-archive@0.1.5
├── ...

npm ERR! missing: hexo-renderer-marked@^0.3.0, required by hexo-site@0.0.0
...
```
