---
title: Hey, what's up? oh,can't help
categories:
  - Life
  - Diary
date: 2017-08-28 00:08:35
tags: Wedding
---
## 2017-08-28
My once best friend got married yesterday. I come from Beijing to accompany her, which is my intention, but things seems doesn't work, she and I are in a quiet tense relationship.
<!--more-->

I tell my close friend that I was disappointed with her several times, which must have caused a bad impression of her to them.

The first night when I come here in Guangzhou, she and her boyfriend who will be husband in the next day invited me included and her families and their friends for a meal, she shelled the first shrimp and gave me it, which her elder brother reminded me that with her close family at present she gave shelled the shrimp for me. But I can't looking into her eyes, keeping avoiding eye contact with her.

You are too cruel, you to my good, but later, you are really trying to evade me.

The wedding was held yesterday night, the look on her face tells us that this was the greatest day of her life. I had a feeling at that moment that something is meant to be.

We went to the hotel to have rest. The bridesmaid told me that my friend was frustrated and sad the night before mainly because of me.

## 2017-08-30
Suppose both of us are looking for the reason why the relationship between me and you turns out to be a mess recently.

I have to say I am always the one who is wondering the real answer, because I, with a humble attitude, can see things in a reasonable way and emotion control, but you, I don't know, you are hiding your true heart from me, not willing to let me know what you are thinking actually.

We both know there is serious problem between our friendship, we try to start a talk which breaks up quiet easily. We should have tried harder to give a chance to show our true heart. Maybe I am too reasonable with our relationship, with less blind love, I become cruel with you, you said some of words I said broke your heart, at that moment, you wished that we have never met, I couldn't apologize for that. No matter what, I don't regret for our past experience.

You feel our heart are departing away, so do me. You think that I don't care for you as before, yes, It's true.

Human mind seems to remember hateful things which once made our heart cold easily,on the contrary, the effect of happiness which warmed our heart become nothing.

Thinking the difficulties you have make you annoyed and you have no idea what to do with it, and you avoid thinking it as much as possible. I choose to look the trouble in
the face, the feeling of desperation and solitude from which I suffered too many times and won't disappear in the future, now I am figuring out a way to get along with it.

As for your marriage, you made the most reasonable choice at the present situation, best for yourself and your family.

I loved you, and I will never turn my back to you, I escape from being too enthusiastic about you, so I avoid eye contact and words, because once I expose my heart to you, the happiness at the beginning would soon become sadness lasting for long time.

My dear, I want to tell you, your behaviour make me worried for you, yes, you are being spoiled with love now, which is fortunate, you need to cherish it not just with words. If you pay back with him your body and appearance instead of your charming personality and excellent ability, some day he will be tired of the life with you.

Me and you remain hope for our relationship. You still rely on me even if you once thought I broke your heart and were disappointed with me.

Monster hides in details, you promised me you will wait me downstairs in 7:00 am, I come down time, and you were not there.

Dear, we are meant to depart just like how we meet and we can do little to prevent it from happening.

![](https://media.giphy.com/media/XWPshra1fu54k/giphy.gif)
