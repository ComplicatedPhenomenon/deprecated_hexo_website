---
title: A milestone in my life
categories:
  - Life
  - Diary
date: 2017-12-06 08:22:37
tags: Career
---
![](http://img.mp.itc.cn/upload/20170704/335f7cca15d24afd84489c40a45d1169_th.jpg)

When I know the result that they decide to give me the opportunity of being a PhD student.  I couldn't believe it.<!--more-->Out of five candidacies (including one from Peking University, one from Beijing Normal University and the other two from China Science Technology University,　and me), I am the most potential one.

It's totally a Cinderella turns into a princess from a certain perspective. I never expect it to happen. I heard the result yesterday afternoon, when I watch the MV of the music Dream If Possible, then my story become a detail one.

Unlike the main character in a film about encouragement. I was never that kind of tough and that kind of brave. I failed several times and missed the opportunity of being able to go to a promising platform in every vital moment. Along the way, I thought about giving up my professional career several times.

In the meantime, I pay attention to what brings me feeling of happiness and fulfillment, that happens so naturally, and I am free to do what I want for about six years or so.

I am far away from perfect because of my very limited ability. I possess some virtues which are shining.

The way ahead is still long. Furthermore, nothing important changed for my life changed.  At least, it's a happy thing.
