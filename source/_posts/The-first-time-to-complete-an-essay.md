---
title: Experience of composing my first paper
date: 2018-04-04 14:25:35
tags: Writing
categories:
- Notes
- Essay Writing
description:
mathjax: false
---
Things sometimes don't work in the way as we expected. What I planed goes one way and reality goes another way.
* Too long. A complex repetitive work was not supported to delay.
<!--more-->

[Advice from Editors](https://www.nature.com/articles/d41586-018-02404-4)

Little Progress! Desperate!

Such a Failure experience. Glad I put an end to it.

Such a pity I don't come through the whole process.

* How to make plans?
* How to make everything you choose to do worthy of your time?
  * Assign meaning to the question, and your own question.
  * Be stubborn to solve it.
* To make sure you stick to the plan, Record your daily life show it to people on the Internet.

* Reading fruitful material.
  After reading the book *Hacker and painter* I thought it is worth reading, you tear up the thought what you used to think and refresh your mind.

Remains unfinished...

![](https://media.giphy.com/media/rs3PtuQlKs1zy/giphy.gif)
