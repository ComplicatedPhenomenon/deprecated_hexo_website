---
title: ROOT
date: 2018-06-15 06:25:14
tags: data analysis
categories:
  - Technology
  - Tools
description: A modular scientific software framework. It provides all the functionalities needed to deal with big data processing, statistical analysis, visualisation and storage. It is mainly written in C++ but integrated with other languages such as Python and R.
mathjax: false
---
![](https://d35c7d8c.web.cern.ch/sites/d35c7d8c.web.cern.ch/files/website-banner-allnew-croped_3.png)
We are certainly unwilling to learn a heavyweight software from scratch. We use ladder to access to a certain level as soon as possible.

Going through these tutorial notebooks helps to form an overview.

 <!--more-->


To obtain the ROOT sources you can either download a specific version from the download page or access the Git repository directly. You can also browse the repository directly.
## Documentation structure
  * Reference Manual
  * User's Guides. There are several guides and manuals depending on the version of root. [...](https://root.cern.ch/root-user-guides-and-manuals)

  * HowTo
    * Developer's Tips
    * Graphics and GUI
    * Histograms
    * Input/Output
    * Jupyter Notebooks
    * Language Bindings
    * Legacy
    * Linear Algebra
    * Multicore
    * Trees
    * Type System
    * Virtual Monte Carlo
    * Writing a Graphical User Interface

  * Building Root
  * Tutorials
  * ...
  * FAQ

There are several ways to use ROOT, one way is to run the executable by typing root at the system prompt another way is to link with the ROOT libraries and make the ROOT classes available in your own program.

## Source distribution and binary distribution
[Binary vs. Source Packages: Which Should You Use?](https://www.makeuseof.com/tag/binary-source-packages-use/)
> Installing programs on Linux is usually quite different to installing on Windows. Rather than get the installers off a vendor’s website, the files are retrieved from a repository of programs, usually tailored for your operating system.

Source distribution
* root_vx.xx.xx.source.tar.gz

Binary distribution
* root_vx.xx.xx.Linux-ubuntu18-x86_64-gcc7.3.tar.gz
* root_vx.xx.xx.macosx64-10.13-clang91.tar.gz
* ...

## Setup the environment to run
```
source /path/to/root/bin/thisroot.sh
```
source is a bash shell built-in command that executes the content of the file passed as argument, in the current shell. It has a synonym in . (period).

Be careful! `./` and `source` are **not quite the same**.
- `./script` runs the script as an executable file, launching a **new shell** to run it
- `source script` reads and executes commands from filename in the **current shell** environment

Note: `./script` is not `. script`, but `. script` == `source script`

```
$ chmod +x thisroot.sh
~/Packages/root/bin$ ls
~/Packages/root/bin$ ./thisroot.sh
./thisroot.sh: line 33: ${(%):-%N}: bad substitution
```

The `source` command can be abbreviated as just a dot (`.`) in Bash and similar POSIX-ish shells. However, this is not acceptable in C shell, where the command first appeared.

## ROOT and Jupyter notebook
ROOT is well integrated with Jupyter Notebooks, both for what concerns its Python and C++ interface.

PyROOT+IPython Jupyter Kernel
* All integration is triggered by the import ROOT statement is triggered by `import root`

C++ ROOTbooks

ROOT Jupyter Kernel allows to have C++ ROOTbooks.
* Functions and macros can be declared within cells marked with the %%cpp -d (or -a) magic.
* Include statements (#include "myheadeer.h") cannot coexist with other statements unless the cell is marked with the %%cpp -d (or -a) magic.

### Jupyter Notebook with ROOT C++ Kernel died and couldn't restart

```
The kernel has died, and the automatic restart has failed. It is possible the kernel cannot be restarted. If you are not able to restart the kernel, you will still be able to save the notebook, but running code will no longer work until the notebook is reopened.
```
>  We distribute for the moment only Python2 based binaries which are not compatible with systems with only Python3 available.

**Q**: Why a C++ kernal has something to do with Python version in JN.

*Don't rush to test whether the answer works out or not, make sense of it in your mind at first.*

For example, Now you know you need a python2 based environment ROOT C++ Kernal. That means when you want to run ROOT in Jupyter notebook. you launch a jupyter based on python2 too.

... Still don't know what is happening. Look up the [official documentation](https://jupyter-notebook.readthedocs.io/en/stable/config.html).

It may relevant with the setup of environment variables.
## Environment variables
* PATH  specify executables
* LD_LIBRARY_PATH specify libraries.
* ROOTPATH
* ...

## .rootrc

The behavior of a ROOT session can be tailored with the options in the .rootrc file. At start-up, ROOT looks for a .rootrc file in the following order:
*  ./.rootrc (local directory)
*  $HOME/.rootrc (user directory)
* $ROOTSYS/etc/system.rootrc (global ROOT directory)

## Tree

Contents to be continued...

## Doxygen
Learn to auto-generated efficient documents.

+ [ ] How the code is structured?


![](https://media.giphy.com/media/1miiQ53ITadPAIkH7N/giphy.gif)
