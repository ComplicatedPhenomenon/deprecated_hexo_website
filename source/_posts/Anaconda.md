---
title: Anaconda
date: 2018-02-11 20:07:10
tags: environment management
categories:
- Technology
- Integrated Platform
description: A data science platform is a software hub among which all data science work takes place. Clearly, Anaconda is similar with MICROSOFT R, Wolfram Mathematica ROOT among others. 
---
## Why do we need isolated environment for each project?
Say you have multiple projects and they all rely on single package, take *Django* as an example, each of the project may be using a different version of *Django*. Now if you go and upgrade that package and your global site packages then it break a couple of your websites that might be what you want to do. It would be better if each of these projects had an isolated environment where they had only the dependencies and the specific packages they need and the version they need.

[conda cheat sheet : A quick start](https://conda.io/docs/_downloads/conda-cheatsheet.pdf)

<!--more-->

## Anaconda distribution

Anaconda® is a package manager, an environment manager, a Python distribution, and a collection of over 1,000+ open source packages. It is free and easy to install, and it offers free community support.

![](https://2s7gjr373w3x22jf92z99mgm5w-wpengine.netdna-ssl.com/wp-content/uploads/2017/08/anaconda_sheet.png)

After you install Anaconda or Miniconda, if you prefer a desktop graphical user interface (GUI) then use Navigator. If you prefer to use Anaconda prompt (or Terminal on Linux or macOS), then use conda. You can also switch between them.


* anaconda-navigator GUI

  Navigator is an easy, point-and-click way to work with packages and environments without needing to type conda commands in a terminal window. You can use it to find the packages you want, install them in an environment, run the packages and update them, all inside Navigator.

  The following applications are available by default in Navigator:
  * JupyterLab
  * Jupyter Notebook
  *  QTConsole
  *  Spyder Scientific PYthon Develop EnviRonment

    Powerful python IDE with advanced editing, interactive testing, debugging, introspection features.
  *  VSCode
  *  Glueviz
  *  Orange 3 App
  *  Rodeo
  *  RStudio

Packages installed into a global environment are available to all projects that use that environment, conflicts may occur when two projects require incompatible packages or different versions of the same package.

Types of environments
* Global environment
* Virtual environment
* Conda environment

[Do I need virtualenv?](https://stackoverflow.com/q/9410800/7583919)
## conda versus virtualenv

[Managing Python Environments](https://docs.microsoft.com/en-us/visualstudio/python/managing-python-environments-in-visual-studio)

### virtualenv
```
$ virtualenv MGRunningEnv
Running virtualenv with interpreter /usr/bin/python2
New python executable in /home/wm/MGRunningEnv/bin/python2
Also creating executable in /home/wm/MGRunningEnv/bin/python
Installing setuptools, pkg_resources, pip, wheel...done.
```


### conda qucick statrt
```
$ source activate WebFramework
(WebFramework) $ source deactivate
$ conda create --name MGENV python=2.7
$ conda env list
```
## IRKernel in Anaconda
The Anaconda team has created an “R Essentials” bundle with the IRKernel and over 80 of the most used R packages for data science, including dplyr, shiny, ggplot2, tidyr,caret and nnet.

Downloading “R Essentials” requires conda. Miniconda includes conda, Python, and a few other necessary packages, while Anaconda includes all this and over 200 of the most popularPython packages for science, math, engineering, and data analysis. Users may install all of Anaconda at once, or they may install Miniconda at first and then use conda to install any other packages they need, including any of the packages in Anaconda.

Once you have conda, you may install “R Essentials” into the current environment:

```sh
conda install -c r r-essentials
```

## Update and downgrade
Step below might reproduce the error
```
$ conda update anaconda
$ conda update anaconda-Navigator
$ conda install rstudio
$ anaconda-navigator
Segmentation fault (core dumped)
```
**Q**: The reason might cause the problem:

**A** Conflict package version. [...](https://github.com/spyder-ide/spyder/issues/6908)

> Unfortunately RStudio runs in 5.6. The problem has not recurred, but just in case I went ahead and created a separate environment with 5.9.

```
2018-06-10 14:24:51  (rev 4)
...
     matplotlib  {2.2.2 -> 2.2.2}
     pyqt  {5.9.2 -> 5.6.0}
     qt  {5.9.5 -> 5.6.2}
    +_r-mutex-1.0.0
...
```
### Revert to the safe mode
In order to match `spyder` and `anaconda-navigator` with right appropriate package version, We install 'qt=5.9.5' to environment *base*.
```
$ conda install qt=5.9.5
```
Revert to a previous package in Anaconda. [Q&A](https://stackoverflow.com/q/23974217)

### Build a separate environment
```
$ conda create --name RStudioEnv  
$ conda install -n RStudioEnv rstudio
```

Should I create a new environments for a projects.
* Yes
* Then we can export the precise package information, in the meantime, excluding these package information not needed.
```
$ du -h --max-depth=1 ~/anaconda3/
．．．
7.3G	/home/wm/anaconda3/pkgs
1.3G	/home/wm/anaconda3/envs
．．．
9.0G	/home/wm/anaconda3/
```
```
$ conda env export --name py2.7Env -f prerequsite.txt
```

## [The data science path](https://www.anaconda.com/training/)
* The anaconda ecosystem
  * The Anaconda Ecosystem

  * Anaconda Packaging

  * Python Data Science Toolbox (Part 1)

    This course covers how to write your own functions in Python to solve problems that are dictated by your data. You'll come out of this course being able to write your very own custom functions, complete with multiple parameters and multiple return values, along with default arguments and variable-length arguments.
  * Python Data Science Toolbox (Part 2)
* Data import and export
  * Importing Data in Python (Part 1)

    This course demonstrates the many ways to import data into Python: from flat files such as .txts and .csvs; from files native to other software such as Excel spreadsheets, Stata, SAS, and MATLAB files; and from relational databases such as SQLite & PostgreSQL.
  * Importing Data in Python (Part 2)

    This course extends the knowledge gained in Part 1 by showing you how to import data from the web and how to pull data from Application Programming Interfaces (APIs).
  * Introduction to Databases in Python

    In this course, you'll learn the basics of using Structured Query Language (SQL) with Python.

* Data manipulations & analysis
  * pandas Foundations

    This course will teach you how to use the industry-standard pandas library to import, build, and manipulate DataFrames.
  * Manipulating DataFrames with pandas

    This course will show you how to leverage pandas' extremely powerful data manipulation engine to get the most out of your data. You will learn how to tidy, rearrange, and restructure your data by pivoting or melting and stacking or unstacking DataFrames.
  * Merging DataFrames with pandas

    This course will teach you the act of combining, or merging, DataFrames, an essential part of any working Data Scientist's toolbox. You'll hone your pandas skills by learning how to organize, reshape, and aggregate multiple data sets to answer your specific questions.
  * Cleaning Data in Python

    This course will equip you with all the skills you need to clean your data in Python, from learning how to diagnose your data for problems to dealing with missing values and outliers.

* Data visualization
  * Introduction to Data Visualization with Python

    This course extends Intermediate Python for Data Science to provide a stronger foundation in data visualization in Python. Topics covered include customizing graphics, plotting two-dimensional arrays, statistical graphics, and working with time series and image data.
  * Interactive Data Visualization with Bokeh

    In this course, you will learn the fundamentals of Bokeh, an interactive data visualization library for Python (and other languages!) that targets modern web browsers for presentation. You will create versatile, data-driven graphics, and connect the full power of the entire Python data science stack to rich, interactive visualizations.

* Statistical analysis
   * Statistical Thinking in Python (Part 1)

     Drawing conclusions from your data hinges on the principles of statistical inference. In this course, you will start building the foundation you need to think statistically and speak the language of your data.
   * Statistical Thinking in Python (Part 2)

     In Part II, you will dive into data sets, expanding and honing your hacker stats toolbox to perform the two key tasks in statistical inference, parameter estimation and hypothesis testing.

* Machine learning
   * Supervised Learning with scikit-learn

     In this course, you'll learn how to use scikit-learn—one of the most popular and user-friendly machine learning libraries for Python—to perform supervised learning, an essential component of machine learning. Using real-world datasets, you will learn how to build predictive models, how to tune their parameters, and how to tell how well they will perform on unseen data.
   * Unsupervised Learning in Python

     This course covers the fundamentals of unsupervised learning using scikit-learn and SciPy. You will learn how to cluster, transform, visualize, and extract insights from unlabeled datasets.
   * Machine Learning with the Experts: School Budgets

     This course focuses on a case study related to school district budgeting, based on a machine learning competition on DrivenData.

* Data at scale
   * PySpark
   * Parallel Computing with Dask

![](https://media2.giphy.com/media/11MOVEfCrUx87m/200w.gif)
