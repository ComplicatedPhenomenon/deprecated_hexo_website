---
title: Apache
tags: big data
categories:
  - Technology
  - Integrated Platform
description: Get an impression on hadoop and spark
date: 2018-06-15 06:25:33
mathjax: false
---
Apache Software Foundation

2 Apache Projects
* Hadoop
* Spark
<!--more-->

## Hadoop installation [...](https://www.digitalocean.com/community/tutorials/how-to-install-hadoop-in-stand-alone-mode-on-ubuntu-16-04)

Hadoop running in single node mode

* Step:  verification
In order to make sure that the file we downloaded hasn't been altered, we'll do a quick check using SHA-256.

```
$ shasum -a 256 hadoop-2.8.4.tar.gz 6b545972fdd73173887cdbc3e1cbd3cc72068271924edea82a0e7e653199b115  hadoop-2.8.4.tar.gz
$ cat hadoop-2.8.4.tar.gz.mds
SHA256 = 6B545972 FDD73173 887CDBC3 E1CBD3CC 72068271 924EDEA8 2A0E7E65 3199B115
$ cat hadoop-3.1.0.tar.gz.mds
SHA256 = 670D2CED 595FA42D 9FA1A93C 4E39B39F 47002CAD 1553D9DF 163EE828 CA5143E7
$ shasum -a 256 hadoop-3.1.0.tar.gz
644db95bac41538f21f0a4112a0921f4425409fdccaac3f9e5c0a5284bea2725  
```
Here we verify 2 different version of hadoop and you can see not all mirror repository stay the same with original one.

Rename our directory to just hadoop and move it to a better home.
```
$ mv hadoop-2.8,4 hadoop
$ mv hadoop /usr/local/hadoop
```

![](https://media.giphy.com/media/frZXpAY6PltKVreyYc/giphy.gif)
