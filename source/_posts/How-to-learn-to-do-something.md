---
title: How to learn to do something?
date: 2018-04-04 11:50:39
tags: Methods
categories:
- Life
- Philosophy
description: Think over the embarrassing moment before and learn form that! When you speak nothing when they give you the opportunity to argue you for something, when you even can not sleep well and become mad about the hopeless situation, when you ...
mathjax: false
---
Some bad habit that I am struggling with.
* Careless with
  * Easy thing.
  * Complex thing.  
* Logic chaos
* Ignore the plan when face the unforeseen difficulty.
Do Remember! Baby Step is precious,After all, that's a good start.
<!--more-->

### Direction and Methods
As for my perspective about Direction and Methods. I will show you a case.

When I refer to *difficulties*, I mean real that is real tricky, in order to get over it, you need to make lots of efforts, it is not something you can easily handle.   

If you always do your work independently, without frequently ask someone for a favor so to speak, then when you encounter difficulties and get stuck, you will be frustrated at first. Even worse, after several times of trying, you will be running from fear for the rest of the time.

For example, I always calculate something manually, it is a tedious work. I am not sure whether the result is correct or not at last. Then I just leave it behind. As I realize things can not go like this any longer. I look for software that can check my work and do something even much more laborious. Then I realize that lots of problems I can think of were addressed already in a clear and professional way. I can look up the literature and hit on the road without being stuck easily.

I find a collection of article introducing several popular software widely used. *[Most Cited Computer Physics Communications Articles](https://www.journals.elsevier.com/computer-physics-communications/most-cited-articles)*

As expected, with the aid of software which ensure me that I am right, I can do more work than ever.

Comparing the numerical result give by the program I wrote with the outcome from an software written by many talented people, I efficiently complete my work and I will own the power more than most individuals got!  

### Believe you can change
> If I were placed in a helpless situation, I would be hysterical.

Sometimes I got a strong feeling that I need a breakthrough, To overcome my weakness which cause me didn't become a real excellent person now and will prevent me from being one of the very best in the future.

The good news is someone points it right into my heart. [Believe you can change](http://www.aaronsw.com/weblog/dweck).

You knew or you will see, hard work doesn't mean you will get what you think you deserve. You need to think about the strategy, that is not about tricks but courage. Some things will show when you make a choice, or when you stuck in trouble, or when you are sad and confused, or even worse, something horrible unfortunately happened to you, That you are a visionary, that you are a growth mindset, that you will make it eventually, that you are tough enough to not let it beat you down.

### Make a plan for during getting a PhD.
Something you should know in advance.
* Know how the real work is done. It contains tedious and laborious work.
* Know what to do when you quit.
* Know in what way you should look at you didn't make it.

![](https://github.com/ComplicatedPhenomenon/BuildABlogSite/blob/master/source/uploads/astrophysicist.svg)

In case to modify this figure in the future, upload the raw code here.
```
digraph G {  
label = "My way to become a astrophysicist";  
node [shape = record];  

A  [label = "Data scientist", color = red];

B1 [label = "Statictics", color = green];  
B2 [label = "Astronomy", color = green];
B3 [label = "Reasonable Life", color = green];
B4 [label = "Composing", color = green];
B5 [label = "Programming", color = green];
B6 [label = "Presentation", color = green];

B1C1 [label = "Foundation"];
B1C2 [label = "Algorithm"];
B2C1 [label = "Arxiv"];
B2C2 [label = "Classical Books"];
B3C1 [label = "Speaking fluently"];
B3C2 [label = "Exercise"];
B3C3 [label = "Travel"];
B3C4 [label = "Read a lot of books"];
B3C5 [label = "Just keep \nin \ntouch with family"];
B4C1 [label = "Daily writting"];
B4C2 [label = "Pick a right \nproblem"];
B5C1 [label = "I got this"];
B2C1D1 [label = "Think hard"];

A->{B1;B2;B3;B4;B5;B6}[color="#FFFF00"]
B1->{B1C1;B1C2}[color="blue"]
B2->{B2C1;B2C2}[color="#00F5FF"]
B3->{B3C1;B3C2;B3C3;B3C4;B3C5}[color="#0000CD"]
B4->{B4C1;B4C2}
B5->B5C1;
B2C1->B2C1D1;
B1C1->B1C2;
B2C1->B2C2;
}  
```
#### Some advice
[Ruyuan Zhang](https://www.zhihu.com/question/47646734/answer/116352789)

>1. 对大部分人来说，博士毕业的难度远远小于把科研当成自己的饭碗。
2. 导师直接决定了你博士期间的生活质量，科研质量，未来发展，是最最重要的因素，至于学校，地方等因素都往后排。
3. 大部分博士生还只停留在好好做实验，好好完成老师布置的任务的心态上，或者在愿望层面，啊我喜欢这个学科我想未来好好做科研。只有极少数人在技术层面知道怎么一步一步做好成长为大牛。
4. 接上，如果想把科研当做未来饭碗，你可以不喜欢科研，但是你不能不懂技术层面道路怎么走。
5. 做科研和创业一样，只有极少数人能成功。
6. 接上，做科研和创业一样，底线是survive，少谈什么大理想，少谈什么为学术前沿以及为人类发展做什么大贡献，最起码做到在学术圈里survive，找到工作。就像创业最起码底线是survive，别倒闭，这个都做不到那些所谓的颠覆这个商业模式，改变那个商业生态都统统是扯谈。
7. 任何事业，包括科研，都是平衡各方面看似冲突的利益关系，牛人总能在理想和现实之前找到一个很好的平衡点。卢瑟永远在抱怨，觉得哪哪都不如意，永远不找自身的问题。
8. 注意保护和积极锻炼身体，多活几年比多发几篇paper重要的多。

[何史提](https://www.zhihu.com/question/26904378/answer/36172866)

> 1. 年轻时，以为做理论物理是做应用数学，后来被教授们重重教训。面璧思过后，决定洗心革面，做个有图象的人。后来我明白了，找工作时，其实就是做售货员，出售你做的那条数学问题。「为什麽你算这个？」没有好的物理触觉，答上来便支支吾吾。
2. 立志做理论物理的同学，本科时都是GPA爆灯之优秀学生；到了研究院，身边有一堆不过二十岁的神童也算了，同学们都是同样的牛，优越感没有了，那个心理落差一开始很难接受。但后来学会了：没有跟人家比较的必要，做好自己的问题，多看文章，观摩人家的工作。
3. 年轻的人总爱做最新最热门的东西，人做我又做，使得我们张张刀无张利，「坐这山，望那山，一事无成。」其实，还是专心做好自己的问题。

[苏远](https://www.zhihu.com/question/63250362/answer/215680413)

>实际上，任何一个行业需要的高端人才数量都是一定的。前5%的人，抢前1%的工作，前10%的人，抢前5%的工作。剩下90%的人，写写Web，App，Server。。。industry并不需要那么多搞Deep Learning，TensorFlow的人。只有5%的工作需要前20%的能力，并且都被前10%的人抢去了。导致的结果就是，前20%的程序员和前50%的程序员，在干的活上并没有什么区别。要是成不了前5%，你科班学的东西，大多数没什么卵用。那么多科班出生的生物PHD，数学PHD，找不到工作转了CS，是因为他们太弱吗？屌如张益唐，没搞出孪生素数猜想之前还不是在Subway打工，他怎么不找份能用上他数学知识的工作？又有多少个张益唐，终其一生都没做出有意义的课题，最终默默无闻。前段时间Google被fire的那哥们，正儿八经哈佛生物PHD，超出知乎上99%的人了吧，他也没找份用上他生物知识的工作。生物狗，数学狗，都已经接受了自己学的东西没什么卵用，CS科班的为什么不能接受？前段时间CS势头太好了，让很多CS的以为是靠自我奋斗，一点也不考虑历史进程。风水轮流转，说不定20年以后，生物狗上知乎提问，"本科学生物的人找工作的时候发现工作被CS转行的人抢了很多，该如何调整心态"。大部分人的人生都是失败的，能发挥自己全部才能的人只是少数。习惯这一点，人生会轻松很多:)


### Astronomy Preparation
* Reading first-hand paper to get an impression. [Arxiv](https://arxiv.org/abs/0907.0869v1).
* Be proficient with the relevant tools.
* Be intuitive with problems.
* Take the hands-on practise

Search *Astrnomy* and *Astrophysics* in Github. Then you will get the most relevant materials. Meanwhile, you are able to find people who also involve in the projects.

[Special Topics in Astrophysics: Statistical Methods](https://kipac.github.io/StatisticalMethods/)
### People whom I learn from
* [Mike Boyle](https://github.com/moble)
* [Jonah Miller](https://github.com/Yurlungur)

  Powered by QuickLaTeX
* [Vijay Varma](http://www.tapir.caltech.edu/~vvarma/)

  Design: HTML5 UP Background image credit: Aleks Sennwald for the New Yorker.
* [Jason K. Moore](https://github.com/moorepants?tab=repositories)
* [Thomas Robitaille](https://github.com/astrofrog)

  I was formerly an astrophysicist, and from 2011 to 2015 I led a research group at the Max Planck Institute for Astronomy in Heidelberg, Germany. As a researcher, I worked primarily on analyzing and modeling large sets of observations that trace star formation within the Milky Way. For my modeling work, I developed Hyperion, an open-source 3-d Monte-Carlo Radiative Transfer code.
* [karenyyng](https://github.com/karenyyng?tab=repositories)

  Data scientist who solves problems with machine learning, statistics & big data tools

* [Mario Juric](https://github.com/mjuric)

  This website keeps summaries and pointers to various papers, results, code, and talks related to my group’s research.

  © 2016 Mario Jurić · Powered by the Academic theme for Hugo.
* [Dan Foreman-Mackey](https://github.com/dfm)

  Works in Flatiron Institute

  The mission of the Flatiron Institute is to advance scientific research through computational methods, including data analysis, modeling and simulation.
* [David W. Hogg](https://github.com/davidwhogg)
* [Johnny Greco](https://github.com/johnnygreco?tab=repositories)

  A fifth-year graduate student in the Department of Astrophysical Sciences at Princeton University. I search for and study diffuse galaxies, with the goal of understanding what the universe is made of and how this relates to galaxy formation and evolution.
* [Florian Beutler](https://github.com/fbeutler)

  PhD in astrophysics, Bachelor in Computer Science, research in cosmology and machine learning
* [Kyle Barbary](https://github.com/kbarbary)

  I study cosmology using Type Ia supernovae as part of the Nearby Supernova Factory, an experiment designed to study nearby Type Ia supernovae in depth in order to enhance their use for cosmology. In the other part of my job, I write and maintain open-source scientific software (primarily for astronomy research) and investigate ways to make it easier for scientists to use programming tools to accomplish their research.

  Contents © 2016 Kyle Barbary | Powered by Nikola | Theme is Yeti
* [Tom Aldcroft](http://hea-www.harvard.edu/~aldcroft/)
* [Manodeep Sinha](http://astro.phy.vanderbilt.edu/~sinham/)

  © 2008 Manodeep. Adapted from design at Free CSS Templates. Valid XHTML | CSS
* [David Parkinson](https://github.com/dparkins)

   My research area is testing theories of the creation and evolution of the Universe against observational data from telescopes and satellites.

  Proudly powered by WordPress
* [Dustin Lang ](http://dstn.astrometry.net/)
* [supernova neutrinos](http://openmetric.org/)

  I am a physics PhD candidate at University of New Mexico, dealing with supernova neutrinos.

  [Physics Notes](https://github.com/emptymalei/physics)

  I am learning web front end technologies. As a practice, I built some projects for fun.

  powered by Jekyll + Ed.
* [Yücel Kılıç](https://github.com/yucelkilic)

  Proudly powered by WordPress | Theme: Just Write by Ryan Cowles.
* [Adam Mantz](https://github.com/abmantz)
* [Colin ColCarroll](https://github.com/ColCarroll/hamiltonian_monte_carlo_talk)

  He is one of the contribuitors to pymc3
  * PyMC3 is a popular Python framework for building Bayesian models and performing inference on them
  * Helped with first stable release, did major work on Hamiltonian (gradient based) methods, and on improving the test suite

### Projects
* [LOSC](https://losc.ligo.org/about/)

  The LIGO Open Science Center provides data from gravitational-wave observatories, along with access to tutorials and software tools

  Software for working with LIGO data. [link](https://losc.ligo.org/software/)

  LIGO data can be analyzed using libraries in many languages (C, C++, Python, Matlab, etc.
* Root & SWAN
* [User Training in JWST Data Analysis II](https://github.com/taldcroft/JWSTUserTraining2016)

  The purpose of this three-day meeting is to provide training in the the open-source data analysis tools being developed at STScI for use with the James Webb Space Telescope (JWST), as well as for many other optical/IR observatories.

### My PhD project
#### Option 1. Concerning white draft system
My approach would be to single out the $nev$ mass range and the coupliing in the range between $1e^{-11}\text{GeV}^{-1}$ and $1e^{-10}\text{GeV}^{-1}$.

In this case we can determine the degree of linear polarization and match it with data.
(Often we do not have specific information about the orientation of th line of the sight etc, but we can always  assume a distribution and possible correlations, like degree of polarization and luminosity that are unique in this model)

* *SDSS* data release pipeline [latest DR](http://www.sdss.org/dr14/)
  * Data access for *SDSS* DR14 Overview. [Tools](http://www.sdss.org/dr14/data_access/)
    * Tools for accessing the SDSS data: the Catalog Archive Server (CAS), the Science Archive Server (SAS), and direct data file access via rsync, wget, or http.
  * How SQL applies to *skyserver*. Structured query language is a standard means of asking [data from dataset](http://skyserver.sdss.org/dr14/en/help/docs/sql_help.aspx).

* On Axion bounds from magnetized WD. [1105.2083.pdf](https://arxiv.org/pdf/1105.2083.pdf)
  * Author including [RAMANDEEP GILL](http://ramandeepgill.com/index.html) and RAMANDEEP GILL [link](https://phas.ubc.ca/users/jeremy-heyl).


Idea of  Workflow of something.

When your repo only contains the concrete stuff, without any words to make it clear, it won't popular.

* Modeling. [Investigation into the shape of the inner DM halo with stream kinematics](https://github.com/jobovy/mwhalo-shape-2016)
* Techniques. [Extensions to the standard made-to-measure (M2M) algorithm for for full modeling of observational data](https://github.com/jobovy/simple-m2m)

#### Option 2: Concerning Gamma-Hardron collider
The physics goal would be to measure diffuse emission in the galactic plane at high energies using **HESS** data and make an effort towards isotropic gamma ray. In both case, improvements in gamma-hardron separation are crucial, In the case of HESS Data Analysis ...

![](https://media.giphy.com/media/PtSP04mBqkDPG/giphy.gif)
Remains to be written...
