---
title: Matplotlib VS D3 VS Data Visualization in Mathematica
date: 2018-03-28 10:45:49
tags: Mathematica
categories:
- Technology
- Tools
description: Data visualization is a way showing data in a impressive way, moreover, you will form an instinct about the data.
mathjax: true
---

[Mike Bostock](https://bost.ocks.org/mike/), whom I found while looking for the example provided by d3.org.


### Preface
Things could be too tricky if you don't rely on other people's work. You need to rely on great codes and invent brand new wheel.
<!--more-->
If you are only able to use other people's codes, you will be called *Packageman*,*worker*...  Sad! Huh.
So you got to be an expert in something.


Scientific calculations favor [scipy](https://www.scipy.org/about.html).
The SciPy ecosystem, a collection of open source software for scientific computing in Python.

> On this base, the SciPy ecosystem includes general and specialised tools for data management and computation, productive experimentation and high-performance computing. Below we overview some key packages, though there are many more relevant packages.

### Cases of visualization of function and data
You can not load a local picture in markdown
![](http://atlas.cern/sites/atlas-public.web.cern.ch/files/Hgg-FixedScale-Short2.gif)

It's a perfect showcase. I will demonstrate more.
