---
title: Life
date: 2018-01-27 11:30:33
tags: Thoughts
categories:
- Life
- Diary
mathjax: true
---
![](http://webneel.com/daily/sites/default/files/images/daily/12-2013/2-watercolor-painting-by-vilas%20kulkarni.jpg)
It's good for me to practice self-expression


### 2017/03/07

Now I understand that most people have lonely feeling and their own pain. It destroy the majority, help the minority. My situation is getting better, depressed feeling won't control me totally. I get on well with the sense of isolation.

<!--more-->
### 2017/03/08

It's really strange that some people raise evil feelings like jealous or resentment when they see other ones doing meaningful things, I feel like being spied, always finding some people staring at my computer, that's not cool. Okay, if you're nervous and want to have a similar skill which you do not know at now, come and ask me. You're welcome to do that, but when you tell others what I am doing just from what you see, I only found what you said is nonsense. I have faith that you can be a better person, in order to prevent you from letting me done, I won't pay attention to you.

I am not willing to write down anything about how I feel angry about others.
Thank you for walking a while with me, It's a beautiful night, I like your smile, it's sweet, I know I won't be able to speak out the  true strong feeling any time. Your life seems completely irrelevant with me, I hope it is not true.

### 2017/03/12

If I do not value much about how I feel(ignore the low mood and sensitive thought) but focus my attention on completing work(I know I've said it for many times, but it still work well), life would become better. If I really enjoy life one day, I definitely would regret wasting so much time and a vast amount of energy. So , WM, be clever, make sure you're right and then go on even if you're not willing to.


### 2017/03/19

When I sit down and begin to program, so many obstacles I confronted, I easily lost my attention while dealing with those error, then there is a probability that I will go  to bed or turn to easy things avoid the annoying problems. So weak my will is! I won't blame myself of losing patience. Warning is hovering in my mind, I need to get through this process even undergo a little bit of suffering.


### 2017/3/21

I meet something really odd recently, As I am in school, I can have access to  download the most papers for free easily, I heard that people outside of school will have to pay for the articles at a rather high price. Why do I say it is odd?  I've seen the documentary < The Internet's Own Boy:The story of Aaron Swartz\>....


### 2017/3/22

I feel so puzzled that the people once I think I familiar with could be so mean. Luckily, they are not sensitive, they do not know how to efficiently and effectively hurt someone.

For most time in my life, I care about what do I want most. It's not fair for my family, I need to get a job, gathering a mount of money, financially support my mom and dad, create chances for them to fulfill their life.

Life is short, And we always easily let time slip away from our families. They have suffered from the hardship in order to guarantee I have little concern to care about, but they get little payback from me, they are waiting to die, they should have been enjoying their life instead of being worried about my life.

There are always many old people waiting to die no matter in the big city or in the rural place, and when they really pass away, people around them who still have conscious fall into sorrow, especially the children. Step back to say, even the parents still alive, what did the children do, they rarely think about their mom or dad even both living a extremely boring life, have the children experience from the suffering parent's life, have they trying to save them from dying, have they looking back the way how their parents bring them up.


### 2017/3/26

After three days running Mathematica without taking a break, my laptop become slowly yesterday, I don't remember what I was doing, it suddenly get stuck, it's exhausting, I forcefully shutdown the my computer, when it start again, everything is totally in a mass, unlike last time, this time I still can lunch the terminal, and Firefox, but every window I opened leave me no choice, I can close the terminal window with command  `exit`, while others I have no idea of shutting them down, and they all are fixed on the screen, can not be moved, maximize, minimize, also there is no Drop-down list, I feel so frustrated, furious, deeply understand the meaning of saying "It's driving me crazy". I know my desktop environment break down as my software still could run even if at a extremely slow speed. So I install a new desktop environment with command line `sudo apt-get install gnome-shell`, I did it for several times, because I need to set `gnome` as default instead of broken `unity`, I don't remember which file did I modify then when I reboot, The GUI totally disappear, I was lead to console TTY1, I want to set gnome on TTY, clearly I failed, and it's already 0:30 AM, so I went to sleep, today I handling the problem all the day with help from my brother of course, some annoying problems remains, but I drop it down, and carry on the crucial problem.


### 2017/03/27

I am fed up with losing patience again and again when I  come across annoying problems. Get used to lean in pain like Aaron said. It's not a bad thing to challenge a justify challenge, I need to extend my endurance of tough problems.

> If you keep exercising...well. It just keeps getting more painful. When you are done, if you've really pushed yourself, you often feel exhausted and sore. And the next morning it's even worse. If that was all that happened, you'd probably never do it, it's not that much fun being sore. Yet we do it anyway -- because we know that, in the long run, the pain will make us stronger. Next time we'll be able to run harder and lift more before the pain starts.

> And knowing that makes all the difference. Indeed, we come to see the pain as a sort of pleasure -- it feels good to really push yourself, to fight trough the pain and make yourself stronger, Feel the burn! It's fun to wake up sore the next morning, because you know it's a sign that you're getting stronger.

> Few people realize it, but psychological pain works the same way. Most people treat psychological pain like the hot stove -- if starting to think about  something scares them or stress es them out, they quickly stop thinking about it and change the subject.

> The problems is that the topics that are most painful also tend to be the topics that are most important for us: they're the projects we most want to do, the relationships we care most about, the decisions that have the biggest consequences for our future, the most dangerous risks that we run. We've scared of them because we know the stakes are so high, But if we never think about them, then we can  never do anything about them.

So the subject is always mainly about self-improvement.


### 2017/3/29
Tomorrow morning I will present my work to teacher Gao, I haven't prepare a neat file, whatever the format is, the content is intriguing , some problems I haven't solved yet.

* There is a remarkable energy scale $M_{EW}$ , correspondingly there also is a length scale which equal to $\frac{\bar{h}c}{M_{EW}}$. So the obvious Electroweak process exactly happen at this scale.

* Why gravity law haven't been well studied in sub millimeters, except limitations on technology, what else, do the uncertainty principle play a crucial role in the probing process.

### 2017/3/30

I give a brief presentation to teacher Gao only just, he asked me what problems get me stuck now, I said the time spending on the last step is too long, He declaim I must do not have the previous procedure totally stuck, he said 'You have valued many things too much, it's just a tool, and you take short to get it'. So I am supposed to talk on the topic about "Three body phase integration". "If I don't ask you to report about the processing, you are negatively come yourself, it's not bad for for you to be supervised, you should realized the changes of your ability happened compare to two weeks, a month, 3 month ago." he means.

I didn't do well in estimating a time limit to accomplish something, I am not good at strictly obeying plans which I think is horrible. It's good and not, the advantage is you always dive yourself into new things, and you have a surprising discovery, like you did your work in a broad way, the disadvantages you delayed your work until obey the routine. Accomplishing your work on time in a interesting way would be far better, or you must figure a way to stick to plan on schedule and give a talk on progress regularly. It's helpful to set up your confidence, and there is still time left for exploring the interesting area(Finally I find it's not matter what kind of thing I am working on, there is enough time to do these things unless I am truly e).

The bad results generated when you delay work

+ you will doubt your ability of self-leaning and ability of solving problems.

+ It makes you life plan into disorder, and you are in great trouble.

There is always a main task for you,  every time when you done it, many uncertain situations disappear, like when you decide to study abroad, at least 2 good papers is required, so you have no choice but write 2 good papers. Of course you can do more in this process.

* you learn programming, another marvelous legacy human mankind created. Don't stop writing essay, or least take a break for days.

* you read books, don't stop reading the papers in the meanwhile

There is little conflicts between things about self-improvement, come on , make sure you're right and then go on !

My problems is dividing time in a big scale, if i was Attracted to something, I just keep doing it, putting aside my current task, then day days or two passed, I feel satisfied and start think I need a relax, maybe giving myself a enjoy time is a good idea, so my task is delayed longer, when I finally turn to it, a little progress once made, I begin satisfied, then time fly by, here comes the presentation time, sadly, a little progress made at last. Another bad habit is impromptu plan happened to me from time to time, like working a long road by foot, then I feel exhausted  and take a long break to recover.

So I need change these habits intendedly. May be these thing is small and I have been aware of it long before, but I need a strong effort to overcome these bad quiet habits.

### 2017/3/31
I just browse the blogs written by my brother, it's my first time to do so, I even don't know he has such a site as own space, so it's little bit awkward for me when I was reading those contents.

I guess my brother haven't told me for a reason.

We just don't want people know us how hard we are trying to succeed in achieving the aim. The effort we took, the practices we made, we just don't want people familiar with us to see. It's not about self-esteem, it's mostly about realizing our personal ambition, while, unfortunately, most people don't have one, or they can not be count as true friends. Honestly to say, People hardly have a true friend. What friend means here is someone similar like a soul mate.

When you know well the basic knowledge of programming. Then you

* Improve on how to think a problem and solve it. (Master the effective method, theory.)

* Learn to shape your programming philosophy, I mean your style, like how to write good comment, how to arrange your code.

* Participate in a real project, this will help you improve your programming ability.

### 2017/4/1

Early my day begins, I just sit here, feeling neither tired nor sleepy, but my brain is in a bad situation, It didn't function well, I can't focus my attention, it's a little cold in the office, the view outside seems so intriguing. Even I have to solve the problem, I still couldn't gather enough attention to get it done in one vigorous effort.

It's frustrating for me, There is a great possibility if I determined to solve a problem, then I resolve it effectively. I don't know why I just can't focus my attention naturally.

Maybe it's not a suitable time to do mathematics early in the morning.

### 2017/4/4

These two days seems unreal for me, maybe because I sleep for a long time in the day. I accompany my mother today. She is still worried about many things, either do I. I hope she shall have little concern about the families, too much worry is not only useless for others but also harmful to herself, no matter what I say to her, she just can't see it's up to her to make the real change.

I am always missing that special girl without doubt, especially even more in the recent time. Yesterday in the middle of the night, I tell myself, I should just do everything as usual while caring about her, but I can't do it, how can I do everything as usual, in the meantime deeply missing her.

Hey, here, now can anybody tell me what should I do?  

I must have my own answer at least. Only me can make me right, using  whatever it is, experience, knowledge, to analyze my present situation, and then get an answer.

I just call to mind that if my work progress, I feel happy from the bottom of my heart, it's very likely to be the breakthrough rescuing from a life out of breath.

Why? There are several reasons.

* It's the most important thing for me at now, I have a respected supervisor, he encourages me to do well. I will not let him down-that's what I told to myself, actually he is disappointed with me but did not give up in that I have the ability to do it.

* I should at first prove to myself, I have the ability to conquer the difficulties that I confronted, then others know me better.

* Doing well makes me closer with her.

### 2017/4/5

When I first heard that Wentworth Miller suffering from depression for many years, I was shocked meanwhile I think it's understandable, anyone who belong to *LGBTQ* has a great possibility of suffering from depression.

### 2017/4/6

I want tell my brother, my parents, my friends that I love a girl, and I am so eager to tell her all of it, but I don't have the right to break the silence between people's heart, please God! I need an outlet to freely breathe and get rid of depression.

### 2017/4/9
If there is a person who can stand with having a crush on someone , that's me. Nevertheless some time I just feel I can not carry on anymore, I am suicidal, I hope I can find a way out, by self-caring I hope I can maintain a peaceful mood, be a trust-worthy person.

Manchester by the sea, Lee is a suffering guy, losing temper alone in home or around people in the bar, he will punch the window with his bare fist, blood bleeding, anger outbreak, depression, regret tortured him. He still did his duty.

Some time I just think if I were alone in this world,  got no parents, no acquittance, no friends, I will live the way as I like, without any depressing advices given by anyone, but I need teachers and classmates, communication is necessary, it guarantee I make wise choice.

I find problems in me.

* I have a bad relationship with people, there are some one I can trust in some degree.
* I think too much about the phenomenons, It must be wrong, people doing things in their way, not in the way you think they will do.
* I didn't do well as I'd hoped.

### 2017/4/10
After getting a good understanding of a new thing, then choose one suited you and do enough practices to become a master in this area.

### 2017/4/11
It's stupid, I have spent days in the insignificant thing. It disorder my plan. And make me think,
* Girls are more probably being interrupted from their own work

It's okay to experience for one time, that kind of work doesn't suit me.

### 2017/4/12
I googled the difference between Internet and Web and how JavaScript is different from Java.
When you know little about something, some concepts always confused you, and bring you enormous pain. Actually JavaScript and Java are two instinct languages, JavaScript, despite its name, is essentially unrelated to the Java programming language, and sounds like has something to do with the later, it doesn't.

Big news, Someone respond me in Mathematica just a moment ago, he gave me an detailed answer, it means a lot to me, I feel so excited, the last time I have the similar feeling is got an respond email from ZRJ. You really can communicate with professional people from all over the world. It is kind of like living in an idea world where beautiful things come true, a world which people like Tim Bernes-Lee would like to construct.



I hope you enjoy your time.

### 2017/4/19
Today is my birthday. I'm frustrated feeling like being abandoned by my little world.

Only me is responsible for my happiness or sadness. It's nothing about others, that's what I told myself. But I am now out of my mind. Why just my brother can't remember today's my birthday,I'm so disappointed with his icy heart. He is important to me, so my whole sweet mental world collapsed, I was left with no choice but stay strong. I hate this kind of feeling.

I sing "Don't weep on your birthday, don't chock either, Instead of hate, read papers or have a good sleep, you will find it's not a big deal comparing with what's truly important. And When you do a great job, you feel much more being loved, conclusion on feelings is not trustworthy and always changing, so let it go, love your life,  do the great thing, you're welcome."  
:eyes: :sweat_smile:

![](https://media0.giphy.com/media/EEsJ1yCiA5Xfq/200w.gif)

### 2017/4/25
I want to say too much.
* About gain strength from within
 * I look in my diary written about eight years ago, I can see the determination without hesitation and effort I am willing to take. So me when I am around 17 years old, I was reliable. I need to look back at the journey I had taken. Experience is precious for yourself.
* About giving up
  * I want to duplicate my diary in a formal way. Every primary thought I have written was inspired by something. So I will add the original references, recover the facts as far as possible. Cite credible resources. But most of the diary was written in Chinese, my OS is not supplied with Chines input.
* There are many interesting researches we can apply it in resolving many problems.

### 2017/5/3
I pick up reading papers, trying to recall my physical intuition and skills. So I plan to stop a while of other languages
programming except Fortran, I use Fortran to do numerical calculations. The integrand is an analytic expression which is quite long, so automatic
processing is necessary, manually handling is nearly impossible.

It's urgent and important, so it's my primary challenge.

I am gathering the efficient methods of reading paper.

* The effective but not be counted as a clever one is trying every possible way hard. It's tough but I have to undergo through.


### 2017/5/6
It's a big day, another breakthrough between me and my mom. I have no idea why I again mention this, maybe I don't afraid to lose anymore, I want to be myself,

### 2017/5/7
This morning as soon as I woke up, I called my mom, she seemed so disappointed,even worse, desperate. I have to keep telling myself that she is not my entire life even if she plays the role as a mom. I feel heartbroken too, but I won't exert my depression on others too. This totally doesn't have to be like this, there is deep reason behind this ridiculous appearance, if so, I can do little to change the situation now.

If my sexual orientation is not up to me, how horrible it is! Mom, I am being kidnapped by your fragile heart. If there is only one way that do as you wish to satisfied you, It's a pity, I just can't do that, I am the one you give the most love also hurt you most, I am the one lead to your miserable fate, it's not true.
I care about you and want us both be happy, It's not that hard, what you are afraid is fantasy, I told you several time, I understand your reaction, but it's so hard when you keep saying like that.
The biggest obstacles standing between in our way may actually just be in our heads.

It's a long road of hard fighting, may be I break down, or I realize personal fulfillment. Pursuit means to be a struggling process. Achievements never come easy, if it not you who devote, then someone surely does.

> People shouldn't be forced to categorized themselves as "gay","straight".or"bi." People are just people. Maybe you are most attracted to men, Maybe you're most attracted to women. Maybe you are attracted to everyone. These are historical claims-not future predictions.If we truly want to expand the scope of freedom, we should encourage people to date who they want; not just provide more categorical boxes for them to slot themselves into...

> So that's why I am not gay, I hook up with people. I enjoy it, sometimes they are men, some times they're women. I don't see why it needs to be any more complicated than that.


### 2017/5/15
The problems will be addressed in a right way, I will seek for professional help, psychologist, meditate the significant gap between me and my family. Last night I wake up for several times, After doing reasonable analysis, I know I am looking forward, I know it's not a big deal to confess my true feelings and no more likely be a barrier between me and her, we will be great friend I can feel her meaning.

Happened to see this sentence when I was just look for an exact word to express the meaning of voice from inner heart.

> It is still the norm for gays and lesbians to lead double lives, reveling their true feelings only to their closet friends.

Again, One's sexual orientation should never become a focus when being referred by anyone else. For me, I don't dare to say I will be an excellent physicist, I can say I am doing the thing I am good at and will do something good for whom still in need.


### 2017/5/21
*Ipython* is definitely a good programming tool at least for me, a beginner, I always want to write down the lessons I learned directly in the program by making comments, nevertheless, there is only plain text, now I need to worry little. Imagine the programmer is giving a lecture on how to program in Python( We know it's an interpretive language which means you can run it without compiling first) he usually will prepare slides which contains the contents of this and the codes, all of the materials can be strutted in a much more natural way.


### 2017/6/9
I haven't have a chance talk to her for about half a month. It's not a problem since I am concentrating on my study, but a serious problem is I don't figure out what I am about to do with the feelings to her.  

Consider her response as no at first. I told myself to accept it willingly and go on my life strongly.

I am not good enough, no matter for whom I love.  I do self-reflection in order to be better myself.

I am satisfied with the way I am taking of be a professional person. In a right way, I can push my self harder.

* Have a taste of dressing suited clothes.
* Behave properly.
  * Improve oral expression, at least know what am I talking about, if not, do self-control.
* Do more formal reading.
  * > [Unlike reading books and long magazine articles (which require thinking), we can swallow limitless quantities of news flashes, which are bright-coloured candies for the mind](https://www.theguardian.com/media/2013/apr/12/news-is-bad-rolf-dobelli)
* Know how to relax and have some fun.
  * > [How to Focus a Wandering Mind](http://greatergood.berkeley.edu/article/item/how_to_focus_a_wandering_mind)


### 2017/06/14
Hello again, Have you ever noticed the arrogant people who share common space with you who unfortunately have no talents. You should be aware of this and then avoid arguing with them. We stand up with them just because we are so ordinary that we have to stay polite. If we are excellent enough then we can teach them a lesson.

Here is some advices.

[**How to Deal With Self-Centered People**](http://www.huffingtonpost.com/?icid=hjx004)


### 2017/06/16
[Class is a building block in Python...](https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/)

Even though I have used *Mathematica* and *Python* since late last year. Honestly speaking, I haven't explore much either of them.  :sad:
* I have learned the basic of Python and written quiet a few little programs.  Yet no concrete problem being solved via using it
* As for *Mathematica*. I used it to perform symbolic manipulations on algebraic objects in an intuitive way instead of professional approach.

The are abundant resources you can find with *Google*. We didn't do well before because we were struck by the problems which couldn't be solved in an efficient way. No more crap, I am doing a good job now. :fire_work:

I need to accomplish simple projects to sharp my skills. Using *Python* to handle large amount data, draw graphs, To make a noticeable difference compare to time when you know nothing about programming, let it help with your major.


### 2017/7/5
 It has been estimated that 1-3% of China's Internet users use circumvention tools to visit overseas websites. Then the news that China's cracking down on *VPNS* make Great Firewall stronger must affect many citizens. You can list lots of inconvenience and you feel furious, what can we done about it, keep being disappointed about the government? Except that, what we can do?

 We survive, long live and prosper and minimize your loss, finding the replacement of getting over the notorious Greatest firewall  to embrace freedom of accessing the Internet. Google has mirror site, same contents with different *urls*, although some sites still out of reach, better than using *Baidu*. Then we must strength ourself to fight against these stupid regulation. Yesterday when we were young that we think we shall be almighty, discussing whether should give up some new technique since it could be harmful when use it to do bad thing, then today we ban *VPN*.

 There is a time we have freedom, one day, it had been taken away, we are eager of freedom, and fight for it. It's not that bad. If we were born in a country like north Korean, we will used to it, and will live within the boundary drawn by a specific person, then what a disaster it is!

 I have to speak out my viewpoint, if I keep silent on these thing which has little to do with me today, then someday, even I were being treated unfairly no one will care too. The situation is serious but not urgent, although we don't know what it looks like at first, at least we know that what we can see has come through a filter. We know what we can not access to.

 The reason why we should have open access to the Internet is a bit like the philosophy in *The picture of Dorian Grey*, we look into our heart, explore it, accept what it really is instead of keep it away from temptation.

### 2017/8/10
We can connect the mirror site of *google* which we have access to at first, *Aloha Browser* is available for android with built-in *VPN*, we may have been cut off from its official site, so we need to find it from somewhere
Will the whole Internet break down someday and we can do nothing to recovery the lost, if aliens just erase everything connected with Internet,

### 2017/7/15
**Some Thoughts about film Silenced**
> It occurs to me that the reason we are fighting so hard is not to change this world but instead it's not to let the world from the change us.

The reason privileged people like the headmaster can do whatever they wishes without restraint is people who are indirect victims like the boy's grandmother at a disadvantage in money and position connive at them. And the direct victims get few supports.

No one is born as hero, people think they can take care of themselves. Victims the reality is there, no intention to test anyone, as human, people feel they have a responsibility to pursue after the truth, willing to sacrifice for justice in order to make the world a better place,

Many people haven't experience great suffering, they simply live a common and busy life, struggling to earn their own living. They say that they have no extra to care about stranger. Yet how horrible it would be when people are used to the extremely distorted reality.

History show people that victory never come easy, It's through heavy sacrifice. You now are enjoying the rights achieved by previous effort made by predators. Meanwhile harmed by unfair treatment. There is no spider man who always beat the bad guy down and save the vulnerable people.
> First they came for the Socialist and I did not speak out-

> Because I was not Socialist.

> The they came for the Trade Unionists, and I did not speak out-

> Because I was not a Trade Unionist.

> Then they came for the Jews, and I did not speak out-

> Because I was not a Jew.

> Then they came for me - and there was no one left to speak for me.

So that's why people who is not belong to *LGBTQ* take part in gay parade. They know they need to let their voice get heard. They show their attitude toward the act of cruelty.

> Great power comes with great responsibility.

Aaron Swartz is also a political organizer, he use his talent to change the unfair reality, he already have been remembered by history and talented people carry on the same mission.

### 2017/7/22
Looking for a way to let my heart feel comfortable. Sometimes I don't know what it is like to be me, because the words I say were not expressing my meaning, I even don't know what my opinion was. The altitude I hold is not mine either, they all come from the book, from somewhere else. If I were enough honest, then my cognition should always have a citation, they are still not mine, I hardly digest what they feed me and what I read and what I bear in mind.

[Levin](http://president.yale.edu/about/past-presidents/levin-speeches-archive)


### 2017/7/25

I am alone and feel lonely, feeling it's time to hang myself.I feel extremely desperate, and there is few people I want to talk to but don't dare.

**lean into pain**

The procedure of learning a new subject, the battle we intend to defect a bad habit, the struggle we made to have the taste of enjoying life etc, these thing all are about practice.

We fight for the toughest, that what worth to do. If it beat thousands of people down, we must keep conquering it until succeed.

Then it's proper time to introduce "[Lean into pain](http://www.aaronsw.com/weblog/dalio)"

How I can make good use of this method?

I want to conquer depression. I pretend to be interested in what other people said, actually few thing triggered my interests. I feel disappointed with others, and think they did great at the same time. I feel disappointed of myself especially. When I begin a day with my paper instead of learning programming, that day always become a mess, I feel terribly anxious, It turns out it will be a terrific day as keep programming all day, it become a gloomy day if decide to read papers at the very beginning of a day, why things become so?

The problems at hand I have no clue how to solve, always feel it hard to make progress. Push forward everything is hard, I read a lot of articles which happen to come across, which consume large amount of time every day,  distract my attention seriously.

I encourage me from surfing the Internet as I make a little comprehension of a concept, all have a profile impression of some subjects.

I never used to make plan, as I can't live up to the plan's expectation. I always make random arrangement, now it shows that the latter better way for me at the present state. At least I shall feel a sense of

### 2017/7/26
> Because in the long run, the pain will make us stronger. Next time will be able to run harder and lift more before the pain starts.

> If feels good to really push yourself, to fight through the pain and make yourself stronger. Feel that burn! It's fun to wake up sore the next morning, because you know that's just a sigh that you are getting stronger.

### 2017/8/12

I've use *Speaky* to chat with people around the world, my intention is to talk with people, let myself place in a English environment and talk with English speakers about daily life.

When people know nothing about each other, say hi, and find a topic to start a conversation, you have to write something about yourself as a profile if you are not sociable, make it possible for other people to find a topic to start with. Then you can find whether they are interested or not.

I hold the principle that everyone come here to make conversation improving their language ability instead of get rid of personal feeling or flirting someone as masked man.

I met a super nice guy from India, he keep me from being stressed so I can talk freely in an easy way. We both are polite, chat on the Internet make me closer to the real self.

From the afternoon of yesterday, the weather have been doing me a favor. The heavy rain always intrigue my cheerful mood. I lobe to watching rain trickling down on the ground and the lighting in the sky, then comes the rumbles of thunder, enjoy taking a breath of clean air and feel the cool winds blowing in your face.

I am a little sorry to the fine weather, spending excessive time on *Speaky* to chat and wait to chat. Even my forefinger is a little hurt and stiff. This is too much irrational and I stop to write this diary. present state. At least I shall feel a sense of achievement.


### 2017/8/14

Little progress in my research paper, I shall write some thing of *Seven Samurai* to comfort my anxious heart, A really village was looking for trouble by robber, some of the farmers proposed that they should look for warriors to prevent the village from being robbed. Then they go to town and begged for the warrior to help them, they were turned down by some warrior since their little I can see the spirit  and their sense of humor.

![](https://media.giphy.com/media/1zJVCFayH2SZ2/giphy.gif)

### 2017/8/17~/18

Yesterday afternoon, I suddenly want to install the *MadGraph*, it developed in a rapid speed, I installed the compiled binary package [MadGraph5@NLO](https://launchpad.net/mg5amcnlo) I happened to download the binary package, things become easier, with  brief tutorial in hand, I intend to make it work, the first thing which got me stuck for an entire day is to install *ROOT*.

I don't learn the difference between `install from source file` and `install from binaries ` until today.

Use command `export PATH=$PATH:/path/to/this/file` to add the path of a 488 executable program to the shell?

EB: So u want to use it or u want to learn about the source, u don't write the *CMAKE* file, there is little meaning for u to drown in these problems. The problem is caused of you don't have the same environment with the developer's, so that program may depend on some third_party library which you don't have, so you will met errors like [`fatal error: TXMLNode.h: No such file or directory`](https://stackoverflow.com/questions/45725540/fatal-error-txmlnode-h-no-such-file-or-directory). And they already told u [> The "configure" script is known to be broken in ROOT 6. Use the CMake based building method](https://root-forum.cern.ch/t/error-fatal-error-txmlnode-h-no-such-file-or-directory-i-met-while-installing-root/25933)

WM: I want both, I want to get a feeling with installing from source file, then I can update my program by change a little bit instead of download a compressed package which I know little about.

Since these who invent tools get respect. I am more suitable to take part in the team to write program used in theoretical physics doing repeatable calculation which human already get bored with.  


### 2017/08/22

Is time or the human heart strange, I would like to thank XX

It's strange, we came trough a long way and met at some point, I don't know your background and I love you ever since we had a brief conversation which I can't clearly recall now.

Now we seldom talk. My feeling become weak by your calm reaction to my confession and my reasonable restraint.

You got the visa and are heading to California next month, Then I will have to no longer see you for half a year. I worried about my future.

There is little fun in my life, I try to persuade myself to be positive but I fail, I don't want to survive, I want to live.I strongly feel numb and struggle with myself.



### 2017/9/5

It's very likely that you totally have no clue about new things at the very first, after reading some tutorial you will get an idea.

As a good self-learner? What do you need to do ?

You need to think, depict a overall picture in your head to build the main structure. Remember, take the first step then you can ask question the way how you ask a question in SW.you need to tell what you have tried to solve that problem which is tricky to you. [Ignacio Vazquez-Abrams](https://mattgemmell.com/what-have-you-tried/)

* static & shard library

  Static libraries are loaded when the program is compiled and dynamically-linked libraries are loaded while the program is running.

  So the OS need to know where it can locate the library , you can add `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/wm/gsl/lib` in `.bashrc` file, so the OS will find the library eternally.

  * Static library has functionality that bound to a static program at compile time. Every static program has its own copy of library.
  * Dynamic libraries are loaded into the memory and binds at run time. The external functionality is accessed at runtime. This process reduces the overall footprint of memory.

* [ Compile and install software from C source code](https://www.wired.com/2010/02/Compile_Software_From_Source_Code)

* Compile and install software from Fortran source code

Compile the source into a library, let the OS know. It's like you are going to perform a task, and you need several secret weapons, at least you need to know where you can find them when you need to. It means nothing to you even the weapons existed in some place.

### 2017/9/6
I will always remember her, that memory with her will never fade away.

Everything I under through, make me know better who I am and what kind of people I used to be. I believe the true enemy for me is myself, I will never bow to the evil side of myself. When it come to I fall in love with someone and desire to be loved back, I
surrender to that special one. I accept their choice with respect. I just play my part at the very best.

### 2017/09/23
Shooting troubles
* Installing packages with pip3
    `SciPy stack` for `python2`
    `SciPy stack` for `python3`
    I asked the [`question`](https://stackoverflow.com/questions/46377664/command-python-setup-py-egg-info-failed-with-error-code-1-in-tmp-pip-build-dg) on `SO`

    ```
    The directory '/home/wm/.cache/pip/http' or its parent directory is not owned by the current user and the cache has been disabled. Please check the permissions and owner of that directory. If executing pip with sudo, you may want sudo's -H flag.
    ```
* Opera update

    I update my *Opera* browser, this is the most exciting news and simplify bash prompt and add color to it.

    It continually introduced new features. For me, I choose it because its intrinsic *VPN*. Even don't notice the ad blocker it also provide.

* [Cite Stack Overflow discussions](https://meta.stackexchange.com/questions/49760/citing-stack-overflow-discussions)

Directly make a link pointing to a specific part of a web page. You shall see an `share` button in the left conner of every answer, copy that and you can share a link to that answer.

e.g. How to use *bibltex*? This [answer](https://tex.stackexchange.com/a/291781/127717) helped me.

* Configure *Vim* for *Latex* and *Fortran*

    In *latex-vim* environment, how to disable shortcuts. `...` will just represent three dots instead of `\ldots`


### 2017/09/25
**Installing software on ubuntu**

* dpkg `dpkg -i install xxx.deb`

* apt-get
    When meet
    ```
    E: unable to locate package foo
    ```
* pip

The magic behind `configure`, `make`, `make install` [ref.](https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install)

```
tar -xvzf xxx.tar.bz2
cd xxx
./configure
make
sudo make install
```
[what does a typical ,/configure do in Linux?](https://stackoverflow.com/a/2529578/7583919)
[how do i create a configure script?](https://stackoverflow.com/questions/10999549/how-do-i-create-a-configure-script)
* tar
    `tar -xvf xxx.version.tar`
    `tar -xvzf file.tar.gz`
    `./bin/xxx`

By this way, you don't need to manually add *PATH* variable to the OS, unlike install from `.deb` or `.rpm` package
everything is settled down automatically.

[Binary vs. Source Packages: Which should I use?](http://www.makeuseof.com/tag/binary-source-packages-use/)

If you are not lazy or have no wish of contributing to the software, install the software from a binaries package which is already compiled by someone. Versions tailored for different OS, if you want to customize the software to your taste and experience the newest feature, install the software from source Which you will compile on your own.

**Automatic building tools**

*FoBiS.py* need *fobis* just like *make* need *makefile*.
*  [make v.s cmake](https://prateekvjoshi.com/2014/02/01/cmake-vs-make/)
*  [binary package v.s source package](https://unix.stackexchange.com/a/152353/220963)
*  [compiling from source v.s an already compiled package](https://softwareengineering.stackexchange.com/a/167507)


### 2017/10/2
After updated to *Ubuntu 17.04*, I turn to *python3*.

To grasp the skeleton of *Hadoop*, I began with the friendly website *tutorialspoint*.[Hadoop](https://en.wikipedia.org/wiki/Apache_Hadoop)

*Hadoop* has two major layers namely:
* Processing/Computation layer (*MapReduce*), and
* Storage layer (*Hadoop* Distributed File System).
Actually, it also refer to the ecosystem, or collection of additional software packages that can be installed on top of or alongside *Hadoop*.

```
$ hadoop
hadoop: command not found.

$ echo $PATH
/home/wm/.nvm/versions/node/v8.6.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```
PATH tells Bash where to look to find the executables like *ls*
You need to add the path to your executable to **PATH**
```
$ which valgrind
/usr/local/bin/valgrind
$ which atom
/home/wm/softwares/atom/EACCESSfile/bin/atom
```
in **.bashrc** file add

```
# add hadoop,atom, apm etc to PATH
export PATH="/usr/local/hadoop/bin/:$PATH"
export PATH="/home/wm/softwares/atom/EACCESSfile/bin:$PATH"
export PATH="/home/wm/softwares/MG5_aMC_v2_5_5/bin:$PATH"
export PATH="/home/wm/softwares/root/bin:$PATH"
```

### 2017/10/3
I always said Lee has talents in Physics, but never thought was that much impressive. I may better in other area, but in Physics, I respect him as a real tough guy, who is about to make achievements in the future.

* He has great faith in what he is doing, he can stay back for what he can get, but never surrender to what he wants to pursue after.
* He has good taste, because his solid foundation which ensure him to keep sharp intuition with people's idea in theoretical physics.
* Of course he is not perfect, but you should notice what he want to know he comprehend them well, that's enough for a person to find his position and right sword to slay the evil dragon.
I understand little for what a work he had show to me. I was bit shocked, he is in an obviously higher level to do physics, he know how to tackle the problem came across in our way, he searched for previous work already existed, and he reproduce that work, moreover, he goes beyond, he knows he did better than the work done by people who he followed.

### 2017/11/17

I am too excited to calm down. I asked Teacher Gao for recommendation letter. He is such a responsible person, finished it on time, and reminded me when should he send the letter to the candidate school. I said I will upload it myself once he sent it to me. I depressed the eager to have a glance of the recommendation letter when he sent it to me, nevertheless, finally I can't help myself but to have a quick look. His sincere words moved me. I'm really very grateful to his supervision. Thank you is far from enough.


### 2017/12/12
Why people missed out the chance to do the right thing?

Put it in another way, why other people don't find what they want to get, why they don't take their time to think through what they want to get really looks like?

### 2017/12/16

I have seen the movie Interstellar. When I read the story behind how it was come into being, how to flesh out the detail, how the talented people came together to work as a team?

* Change reading speed during reading.
* Translate the original essay into Chinese, then revert it to English, compare it to the original form.

From my words you can judge my language level. Poor ability, couldn't find the accurate words.   

~~Jonathan has little knowledge of science, but he is brilliant and  curious and eager to learn.He spent several months devouring to learn the relevant science and asking the question he met~~
> Jonathan had little knowledge if science, but he was brilliant and curious and eager to learn. He spent many months devouring books about the science relevant to Interstellar and asking probing questions.


~~Chris is occasionally out of mark, but usually right on.~~
> His intuition often off the mark, but usually right on.


~~Chris is down to earth, He has a great sense of wry humor. He reminds me of another friend of mine,  , the inventor of the Intel. They are both completely unpretentious.~~
> Chris is down to earth, fascinating to talk with, and has a great sense of wry humor. He reminds me of another friend of mine, Gordon Moore, the founder of Intel: Both, at the pinnacle of their fields, completely unpretentious. Both driving old cars, preferring them to their other, more luxurious cars. Both make me feel comfortable and ,introvert that I am, that's not easy.

The wheel turns, nothing is ever new.

How could I make sure my belief won't vary? that my mind will always keep sharp? How should I know that I will form a good character?

If the era is crazy, how I can stay awake? (Japanese World War 2, colledge student in cultural revolution)

### 2018/8/23
When I talk on Skype with Professor Horns yesterday afternoon, I was touched by his behaviour and feel ashamed of myself in the time.

For almost 30 minutes, he talk about the physics.

* He treats seriously.  He will argue for every assumption rather than take it for granted, he will consider the detail.  

  I remember he said he is not trying to prove their result and that doesn't make sense. What is that supposed to mean.
  
  He show me how to search the papers, he share the screen with me, that is really nice. Because I can understand him better.

  He speaks English very well. Fluent and diverse usage proper words.

* He always make sense of what his points is.
  He read so many paper, know so well about the previous work, He can evaluate other people's work. Bases on 2 years data, but only 3 sigma credential level.

  $10^{-17}$Gauss, so you should be able to explain the origin of IGMF, in other words, the seed field to generate such a strong MF.

  $10^{-18}~10^{-20}$Gauss. Different assumption do leads a big difference.

  Helicity, IGMF  has a structure .
* He don't say any random thing, he continuously show his mind.  
* He seems so energetic, and so many
It seems that physics become alive through his narrative. I was attracted by his way and this erase my nervous away.

I wish I would be so pure like him, like Miyazaki Hayao, like a good actor, like an artist, and like everyone else who pays attention to what matters to their life.

I should focus on thinking over the problem. I will become talkative about my mind.

He is right. He said I should ask him anything in the project proposal.

This is amazing, I was in touch with people from very far away, I may never could see them, but they help me to understand things, and make sure me I can solve the problem. These encouragement is precious. Make me feel that I'm in an advanced world where people have extra energy to take care of someone who is in need.

I should grasp the opportunity and make the effect shine.
