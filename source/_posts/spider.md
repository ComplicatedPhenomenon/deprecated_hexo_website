---
title: Crawler
date: 2018-07-04 21:45:30
tags: Crawler
categories:
- Technology
- Library
description: Grasp what is accessible to you. It means you have to login otherwise you don't have permission to most information.

mathjax: false
---
Since zhihu produce unbearable amounts of advertisement and I don't know a plug in to get rid of these irrelevant annoying things, I decide to grasp the necessary stuff via crawler. Thankfully I found someone has write such a tool.

<!--more-->

## [zhihu-oauth](https://github.com/7sDream/zhihu-oauth)
### Install
* pip install zhihu_oauth (bad choice)
* python setup.py install (good choice)

### Basic Usage
#### login
* Through command line in python shell as showed in below (Collect user's follower).
* Through adding statement `client.load_token('token.pkl')` into python script.

```

    # coding=utf-8

    from __future__ import unicode_literals, print_function

    import os

    from zhihu_oauth import ZhihuClient


    TOKEN_FILE = 'token.pkl'


    client = ZhihuClient()

    if os.path.isfile(TOKEN_FILE):
        client.load_token(TOKEN_FILE)
    else:
        client.login_in_terminal()
        client.save_token(TOKEN_FILE)
```
#### Collect user's follower
```
(py3.6Env) wm@vampire:~/Playground/Example/PythonProject/zhihu-oauth$ ipython
Python 3.6.5 |Anaconda custom (64-bit)| (default, Apr 29 2018, 16:14:56)
Type 'copyright', 'credits' or 'license' for more information
IPython 6.4.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: from zhihu_oauth import ZhihuClient

In [2]: from zhihu_oauth.exception import NeedCaptchaException

In [4]: client = ZhihuClient()

In [5]: try:
   ...:     client.login('+8615210311877','wmca*******')
   ...: except NeedCaptchaException:
   ...:     with open('a.gif','wb') as f:
   ...:         f.write(client.get_captcha())
   ...:     captcha=input('please input captcha:')
   ...:     client.login('+8615210311877','wmca*******', captcha)
   ...:     
please input captcha:dhbn
...

In [11]: me = client.me()

In [12]: print('name', me.name)
name czfzdxx
...
In [20]: dir(me)
Out[20]:
['__class__',
 '__delattr__',

 ...
 'draft_count',
 'educations',
 'email',
 'employments',
 'whispers']

In []: # 有些属性对应的是知乎类或者知乎类的列表（生成器）。

In []: # 生成器可以通过 for ... in ... 进行迭代。

In []: # 知乎类可以通过连续的 . 操作符进行流式调用，直到获取到基本属性。
In [21]: for following in me.followings:
    ...:     if following.over:
    ...:         print(following.over_reason)
    ...:         continue
    ...:     print(following.name)
    ...:     
孙绿
Eureka
仓皇北顾
...
逸心
Lens
李星河
刘博洋
高科
```
#### Collect a user's and a question's answer .
```
In []: lxh = client.people('li-xing-he-90')
In []: lxh.answer_count
Out[]: 1096

In []: for answer in lxh.answers:          
    ...:     print(answer.content)
In []: question= client.question(263753027)

In []: for  comment in question.comments:
    ...:     print(comment.content)
    ...:     
<p>想知道问题里的计算能力指代的是一种什么样或怎样的能力?</p>

```
![](https://media.giphy.com/media/qfF4Q1q6g4QRW/giphy.gif)
