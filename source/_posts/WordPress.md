---
title: WordPress
date: 2018-05-18 22:46:39
tags: WordPress
categories:
- Technology
- Method
description: Using WordPress as a web application framework
mathjax: false
---
WordPress is open source software you can use to create a beautiful website, blog, or app.
> WordPress powers 30% of the internet.

Before go right into WordPress dashboards, you need to have a few things in place. LAMP as a local server.  phpMyAdmin is a PHP script meant for giving uses the ability to interact with their MySQL database.

![](https://tse3.mm.bing.net/th?id=OIP.hmtxlliesEFAVJ9XkkfbpgHaDQ&pid=Api)
<!--more-->

## Install LAMP and phpMyAdmin
### Basics
Apache is the actual web server software, MySQL is the Structured Query Language for the backend database software, and PHP is the hypertext preprocessor for hosting dynamic web pages using the server-side HTML embedded scripting language.  

MySQL is a database management system that is used by WordPress to store and retrieve all your blog information. Think of it this way. If your database is a filing cabinet that WordPress uses to organize and store all the important data from your website (posts, pages, images, etc), then MySQL is the company that created this special type of filing cabinet. [...](http://www.wpbeginner.com/glossary/mysql/)

**WordPress uses the PHP programming language to store and retrieve data from the MySQL database.** To retrieve data from the database, WordPress runs SQL queries to dynamically generate content.

For users that are not comfortable writing their own PHP and SQL scripts, most web hosting providers offer easy to use web applications to manage databases. One such web application is phpMyAdmin which allows users to manage their database using a web based graphical interface.

### Quick start
Even if you already have all the components installed and **integrated with one another**, they should work with
the examples in this book.
* [How to Install Apache, MySQL and PHP on Ubuntu 17.04](https://www.vultr.com/docs/how-to-install-apache-mysql-and-php-on-ubuntu-17-04)

  * Step 1: Install Apache
  * Step 2: Install MySQL
  * Step 3: Install PHP
  * Step 4: Start Apache and MySQL on boot

* [Installing phpmyadmin apache2 mysql php ubuntu 17.04 17.10](https://websiteforstudents.com/installing-phpmyadmin-apache2-mysql-php-ubuntu-17-04-17-10/)

Before continuing below, please make sure you’ve installed MySQL database server.. To to install it, run the commands below..
```
sudo apt-get update
sudo apt-get install mysql-server mysql-client
```
* Step 1: Install  Apache2
* Step 2: Configure Apache2
* Step 3: Install PHP
* Step 4: Configure Apache2 to Use PHP
* Step 5: Install phpMyAdmin
[How to assign hostname to the web server?](https://stackoverflow.com/q/4991410/7583919)

You can connect to the local server every time since you have executed `sudo systemctl restart apache2` after you have installed `apache2`.
### Some Problems you might wonder
#### About phpMyAdmin
* [How to solve the phpmyadmin not found issue after upgrading php and apache?](https://askubuntu.com/q/387062)

* [How to create an alias for phpmyadmin in apache?](https://stackoverflow.com/q/49803949/7583919)

* [I may reach with localhost/phpmyadmin but fail to login, I have to reconfigure phpmyadmin, reset MySQL password, and then I may login without problems.](https://askubuntu.com/a/175204)

* [/var/www/html](https://stackoverflow.com/a/16197738/7583919)
    ```
    sudo ln -s /usr/share/phpmyadmin /var/www/html
    ```

#### About phpMyAdmin
When I tried to login phpMyAdmin with several user, root, wangmiao(wangmiao04WM&), phpmyadmin(Retrieved while executing `sudo dpkg-reconfigure phpmyadmin` which I think is a MySQL username for phpmyadmin to register with the database server. )
* Can't access phpMyAdmin because of host, username and password. [...](https://serverfault.com/q/259502)

#### About MySQL
* Creating a MySQL Database for WordPress

How to login to the phpMyAdmin interface

> If you don't set username and password for wamp server it never asked for it.

##### Mysql login
* Change `sudo mysql -u root` to `mysql -u root`
[Mysql login](https://stackoverflow.com/a/42742610/7583919)

* Your password does not satisfy the current policy requirements.[...](https://github.com/ShahriyarR/MySQL-AutoXtraBackup/issues/3)

* Adding User Accounts. [...](https://dev.mysql.com/doc/refman/8.0/en/adding-users.html)

### Silly attempting because of my innocence

Since I install MySQL long before. I used to login as `sudo mysql -u root`. I don't know a little behind this. Then I think I create a user `root` after login.

* I can't access to create a database via phpMyAdmin through the user `root`.

 'Access denied for user 'root'@'localhost' (using password: NO)'.[...](https://stackoverflow.com/a/19229842)

 * Reset the mysql root account password.[...](https://dev.mysql.com/doc/refman/5.7/en/resetting-permissions.html)

 * Grant privilege to root. [...](https://stackoverflow.com/q/8838777)

 * Restore user root privilege.[...](https://stackoverflow.com/a/1709138)


mysql mysqld mysqladmin.[...](https://stackoverflow.com/a/22132817)

## Asking a question in a proper way
[What Do You Mean “It Doesn't Work”? [closed]](https://meta.stackexchange.com/q/147616)
## Reinstall MySQL
* Step 1: Remove the annoying old one.
  ```
  sudo apt-get purge mysql-server-5.7 mysql-client-5.7 mysql-common mysql-server-core-* mysql-client-core-*
  ```
* Step 2:Install MySQL client & server. [...](https://www.cyberciti.biz/faq/howto-install-mysql-on-ubuntu-linux-16-04/#comments)

  ```
  |───────────────────── Configuring mysql-server-5.7 ├─────────────────────┐
  │ While not mandatory, it is highly recommended that you set a password   │
  │ for the MySQL administrative "root" user.                               │
  │                                                                         │
  │ If this field is left blank, the password will not be changed.          │
  │                                                                         │
  │ New password for the MySQL "root" user:  
  root1992#R
                                  <ok>
  confirm
  ```
* Step3: Perform a command to secure our MySQL installation
  ```
  $ mysql_secure_installation
  ```

## Where to begin with MySQL?

* [A user friendly website](http://www.mysqltutorial.org/)



## Welcome to WP

Site Title: Holy Grail
Username: JT
password: holgraijt


Unable to create directory wp-content/uploads/2018/05. Is its parent directory writable by the server?

```
$ sudo chown -R www-data:www-data /var/www/html/wordpress-4.9.5/wordpress/wp-content/
```
Go to
```
http://localhost/wordpress-4.9.5/wordpress/wp-admin/
http://localhost/wordpress-4.9.5/wordpress/
```

[How to Find the FTP Username & Password for Wordpress](http://smallbusiness.chron.com/ftp-username-password-wordpress-62694.html)

[ftp server is not localhost it is files.000webhost.com ](https://www.000webhost.com/forum/t/failed-to-connect-to-ftp-server-localhost-21/46522)

[How to set/find ftp username , password , connection type in linux](https://stackoverflow.com/a/6238406)

[How To Find Your FTP Hostname, Username & Password](https://wpsites.net/tools/find-ftp-login-details/)


Step Two - Make a Plan

Based upon the information you've just read, including instructions on installing WordPress, you should have a list of the things you need and things to do. If not, make that list now. You'll want to make sure it includes the following information:

* Website Host Requirements Checked and Verified
* Versions of PHP and MySQL Checked and Verified
* Web Host Compatibility with New Versions of WordPress
* Your Website Username and Password
* Text Editor Software
* An FTP Client Software
* Your Web Browser of Choice

[Setting up an FTP client](https://wpsites.net/tools/setting-up-ftp-how-to-setup-an-ftp-client/)

[How to create an FTP account for your WordPress site](https://themeskills.com/create-use-ftp-account-wordpress/)

[How to set up ftp to use un locally hosted website](https://askubuntu.com/questions/14371/how-to-setup-ftp-to-use-in-locally-hosted-wordpress)

```
Installing Plugin from uploaded file: elementor.2.0.8.zip

Unpacking the package…

Installing the plugin…

Plugin installed successfully.
```

现在还涉及不到FTP client, FTP account, cPanel．等把网站部署到网上服务器时．自然会有那些东西．

* How to create a FTP account
  * cPanel
  * hostgator

  How to transfer a live wordpress website to your localhost manually? View on YouTube

  Web host to local host. ( As a backup for your website.)
* How to Move WordPress From Local Server to Live Site.

## Move WordPress from localhost to a live server
Developing a website locally speeds up the developing process. Once you have  finished your website on your own computer, the next step is to move the site live and share your creation with the world.
* guide [1](http://www.wpbeginner.com/wp-tutorials/how-to-move-wordpress-from-local-server-to-live-site/)

![](https://media.giphy.com/media/yCACLb0AMDRi8/giphy.gif)
