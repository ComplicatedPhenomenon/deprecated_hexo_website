---
title: Airsickness
categories:  
- Life
- Diary
date: 2017-08-03 08:04:07
tags: Feeling
---

The first time to travel by air is very uncomfortable, similar feeling which I had in the playground, then I forget the lesson and did it again, It became a nightmare or a disaster moment for me.
<!--more-->
1.  **Technology**. I keep telling myself, trust human technology, it also means you have faith in  your major， human did lift this nearly a hundred of tons thing, airplane,up to 10 KM of height. They have done it countless times, look,  people around you are all fine,  but it did little effect to ease my nerve.
2.  **Professional**. I met a very dedicated stewardess, she kept reminding others to fasten their safe belt right away when the plane is shaking and she was unable to stand firmly. Maybe it's like anything else, she seldom feels under the water since she got enough training.Nevertheless, I dare to say when the real dangerous come, she will also do a excellent job, forgetting herself in bring service to others.
3.  **Indescribable suffering**. I Could hardly feel my hands, my heart seems will stop beating in no more ten seconds. It's okay with the turbulence, but weightlessness can kill me, so horrible that I think I will faint right away. Every sound seems being amplify in my ear, I just feel so horrible that the struggling won't last any longer since I'm losing conscience.
Children burst into screaming just for fun when the turbulence and feeling of weightlessness come while I was struggling to breathe. Many times I want to press the button to seek for help, but I was too nervous to do so. This kind of phenomenon must be rare, but it's true.

Anyway, the mixed feeling is beyond words. Anyhow, thank god I survive. I think before I make sure my body can take it,  I won't fly again.

[![](https://longliveandprosperblog.files.wordpress.com/2017/08/img_20170802_210121.jpg)](https://longliveandprosperblog.files.wordpress.com/2017/08/img_20170802_210121.jpg)
