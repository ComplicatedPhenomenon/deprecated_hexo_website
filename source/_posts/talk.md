---
title: The Orgin of Ipython
categories:
  - Technology
  - Talk
date: 2017-08-04 13:50:50
tags: Ipython
description: My laborious work was easily beaten down by YouTube's incredible accuracy rate on auto-generated captions.
---
![](https://upload.wikimedia.org/wikipedia/commons/c/cc/IPython_Notebook_Workflows.png)

[surprise from IPython's evolution](https://m.youtube.com/watch?v=g8xQRI3E8r8)

<!--more-->

I watched the video and tried to write down what he said. Maybe someone can help me improve it.

#### Q: You created IPython back in 2001, what problems were you solving at that point?

A: Basically I was trying to switch away from using property tools to using open tools and I want to simplify a lot of my workflows from many many languages to use less, when I discovered Python, I realize it can replace a lot of different tool which just one language can make it easier keep kind of ?move workflow.


#### Q: So you make your own things, right?

A: Yes, but what was missing in Python was a good interactive computing environment. Python has a basic interactive shell, but doesn't fit the kind of workflows that needed in scientific computing when you want to run a script, you want to look at data, you want to plot some variables, you want change the code little bit and keep a very exploratory workflow and so. That being a graduate student looking for excuses not to work on his dissertations and instead to do something more fun were the perfect combination to start writing IPython.

#### Q: What surprise that you feel about IPython evolution?

A: I guess the facts precisely that something began as such a simple personal fix for a problem in my own workflow has grown into such a large project. IPython was actually the first Python Program that I ever wrote and for the looks it's going to be the last I have ever write. Because it began as an afternoon hack and here we are at 13 years later, and now it's a fairly large project. So the fact that would be able to learn from that one use case of a personal scientific workflow and begins to abstract and abstract way and building extremely broad aspect tool now serves other needs including education, publication, scientific computing at that scale has been a really surprising and interesting path that something that was born in such a narrow?? has grown both in scope and in a community sense. That community has been so eager to work with us and adopted it and the projects is now really not my work but actually the product of our entire community of people more talented that I am and who do all hard work.

#### Q:How do IPython Notebook come about?

That was something that the current iteration that everyone uses is about our sixth at attempt at creating a notebook.(Q:Is that right?) yes we have five prior prototypes of various kinds. And that came from tools that existed that very widely used in scientific computing community. I was a heavy user of systems called Maple and Mathematica, both of which have notebooks like environments, the Mathematica once called notebooks, the Maple called worksheets, that combine the idea of having texts, mathematics, code and the results of the code all in one documents, and that's a very natural environment to work in for scientists. So from day one, when I started the very first mini script that was IPython 001 which is about 250 lines of codes are ready to have mentions of mathematics in it. And I was a heavy Maple and Mathematica user so we were after those ideas from early on. But neither of our technology, nor the abstractions, nor our own understanding of our problems were ready in those early 5 prototypes were missed steps but along the way, we learned many many things, and eventually both our capitalizing of kind essential ideas with the framework of the modern web with taking that notion and moving it over to web browser, with a lot of inspirations from project code sage, that build something in 2006 for mathematics, kind of took us to the modern notebook,... so it was a long road but the intention was there from the beginning, and in 2011, Brian Granger who is my Co PI in a lot of projects ... was able to prototype what became the current version over across the summer, joining the technology that were just coming out online, like *WebSocket*, like *jQuery*, like *tornado* and ?, which had the time all very new, but they kind of fit perfectly in having tried So what appears like very new solution to many people is something we actually been work with and thought over 15 years.
...
The intention was there since the beginning as I said,But obviously it was too a big problem for me as a graduate thinking, I want something like that there was no I am gonna do, so the first cut terminal basic environment, but the first prototype 5 prototypes all of which dead ended those dead ends were very productive, they showed us all the wrong decisions and all the things we shouldn't try to do. And also external technology advances the *javascript* were absolutely critical the facts that tools like Gmail showed everyone that you could build highly responsive, highly interactive environments in the web that people would willing to use as data tools. All of the javascript technology behind made huge difference because it was machine ... was really small team to build something that exactly production?? for many scientists.

#### Q:How much programming do you feel that scientists need to know?

It varies quiet a bit depending on exactly what were doing, I think it's important to know that for scientists, programming has very different flavor that adores software developers , programming is inertially exploratory, it's really about solving a question, it's okay to write one scripts, and at the same time, but scientists also need to learn how to gradually understanding when they do need to abstract a little bit, when that data pattern that data set going to be similar in other problems they should not be copied pasted 50 time but they should beginning moving a little bit functions and having a little base finding a way.

#### Q: What you take on the notion of everyone should learn the code?

A: Why do we code? We code because we are trying to communicate with machine which does not have human style natural intelligence how to accomplish task, and in addition I mean a dump machine, in coding we have to clarify to the point that dump machines can understand our thoughts process what it is we are going to do. It means that we have to organize the process in a very precise and unambiguous way, we have to perhaps abstract basics can be done by using the reusable components etc., and that process of logical system organized abstract computational thought that I think is a useful valuable skill in many context in life...
