---
title: Manim
date: 2018-02-11 19:43:15
tags: Manim
categories:
  - Technology
  - Tools
description: Animation engine for explanatory math videos
---
I find an appealing way to show some of the profound concept in math.

![](https://media.mnn.com/assets/images/2017/04/zeta.png.653x0_q80_crop-smart.png)

The way I do with this software somehow represents my general way of learning new things.  

<!--more-->


From [manim](https://github.com/3b1b/manim) repository, Definitely, this software keeps a low profile.

## Installing
Manim dependencies rely on system libraries you will need to install on your OS:

* ffmpeg
* latex
* sox

Then you can install the python dependencies:
```
pip install -r requirements.txt
```

**Notion** : It depends on python2 and the related ecosystem.

## Story behind Manim
[About](http://www.3blue1brown.com/about)
>  It's not that I want to discourage others from doing similar things, quite the contrary, but often my workflow and development with manim can make it more difficult for an outsider to learn than other better-documented animation tools.

Now you know *manim* is a poorly documented tool. This will be a killing problem
if you want to try it out. Meanwhile, you know how this kind of animation
is produced, for example, using a python library.

>   It enforces a uniqueness of style, for example, which is by its very nature a benefit that can't be shared.  

So cool! :smirk:

> I was fortunate enough to be able to start forging a less traditional path into math outreach thanks to Khan Academy's talent search, which led me to make content for them in 2015/2016 as their multivariable calculus fellow.  I still contribute to Khan Academy every now and then, as I live near enough and we remain friendly, but my full time these days is devoted to 3blue1brown.


** What resources did you use to get the mathematical knowledge you have today?**

>  I'm still always learning, and moreover always trying to refine how I learn, and I have no clear answers on what is best. So take anything I say with the knowledge that it should be heavily supplemented with advice that other (wiser) minds have to offer.

> In general, I do think the best way to learn is to emphasize solving problems, rather than reading/watching alone. Math is fundamentally about patterns, and solving problems is a good forcing function for immersing yourself in patterns.
