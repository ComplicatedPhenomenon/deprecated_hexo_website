---
title: About
date: 2018-01-26 19:25:50
---
I am a pig who sometimes wants to fly and never thought that I could make a change with my boring life. Nevertheless, I got a feeling that I can do something to break down the limitation of my imagination.

Thinking in the way as above is easy and it's straightforward to come up with this good idea, but you could still mess it up at last.

![Afraid of flying](https://img1.etsystatic.com/065/0/9774030/il_570xN.797486281_pr3v.jpg)
